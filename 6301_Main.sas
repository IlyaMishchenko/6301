﻿														/* 6302_Main.sas"; */
										/* Bringing All SAS Programms for 6302 Project Together */

%let Path		=	C:/CORP_PRJ/PRJ_imishchenko/Projects/6301/Data;	    		/* Working   Files location */
%let InPath		=	C:/CORP_PRJ/PRJ_imishchenko/Projects/6301/Data/Input;		/* Input     Files location */
%let OutPath	=	C:/CORP_PRJ/PRJ_imishchenko/Projects/6301/Data/Output;	/* Output    Files location */
%let ProgPath	=	C:/CORP_PRJ/PRJ_imishchenko/Projects/6301/Programms;		/* Programms Codes location */

* %let Path		=	/folders/myfolders/SAS_Projects/6301/Data;					/* Working   Files location */
* %let InPath	=	/folders/myfolders/SAS_Projects/6301/Data/Input;			/* Input     Files location */
* %let OutPath	=	/folders/myfolders/SAS_Projects/6301/Data/Output;			/* Output    Files location */
* %let ProgPath=	/folders/myfolders/SAS_Projects/6301/Programms;				/* Programms Codes location */

libname Libr  "&Path";

%let NG = 2;											/* Number of Groups (g = 0 &to &NG) */
%let NV = 4;											/* Number of Visits (i = 1 %to &NV) */

%let ND = 3;											/* Number of Disposition Statuses (d = 1 %to &ND) */
%let NP = 2;											/* Number of Populations (p = 1 %to &NP) */

%let PopFlags = FASFL  PPFL;						/* Patients Populations: FAS/PerProtocol */

*%global	TotD10  TotD11  TotD12					/* Normalization Values =                  */
*			TotD20  TotD21  TotD22					/* Disposition (&d) Totals by Groups (&g), */
*			TotD30  TotD31  TotD32;					/* d = 1 %to &ND, g = 0 &to &NG,           */
*%global	  ND10    ND11    ND12					/* and its Text Representation             */
*			  ND20    ND21    ND22
*			  ND30    ND31    ND32;

*%global	TotP10  TotP11  TotP12					/* Normalization Values =                  */
*			TotP20  TotP21  TotP22;					/* Population (&p) Totals by Groups (&g),  */
*%global	  NP10    NP11    NP12					/* p = 1 %to &NP, g = 0 &to &NG,           */
*			  NP20    NP21    NP22;					/* and its Text Representation             */

%let GName0 = Всего;									/* Group Names */
%let GName1 = F.L.U.Complex;
%let GName2 = Кагоцел;

*proc printto log = "&ProgPath/Log.txt";
proc printto;
run;

%let CurProg = %sysget (SAS_EXECFILEPATH);
%let Time    = %sysfunc(time(),time8.);
%let Date    = %sysfunc(date(),date9.);

%symdel OutFile / nowarn;	/* Prevanting Occasional OutFile-MacroVariable Definition */

ods escapechar="#";

%let TitleSAR =
title
 j = l "#S={font_size=8pt
				borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}
Протокол клинического исследования BIO8-FLU-001
#n Версия Протокола 1.1 от 25.06.2018
#n Статистический отчёт v1.0 от 26.10.2018
#n &OutFile."	/* ! OutFile-MacroVariable should be specified for each printing document ! */

 j = r '#S={preimage="&InPath/_Logo.png"
				borderbottomcolor=black borderbottomwidth=2pt outputwidth=100%}'

  /* ! ' ... "..." ... ' ! */
;

%let FootnoteSAR =
footnote
 j = l "#S={font_size=8pt}
SF-DM&BS-04.03-01 (effective date 01-Sep-2017)
#n Atlant Clinical Ltd.
#n SAS version &SYSVER for &SYSSCP &SYSSCPL
#n Date: &date Time: &time
#n &curprog"
 j = r "#S={font_size=11pt}
Page #{thispage} of #{lastpage}"
;


%include "&ProgPath/Setup.sas";

%include "&ProgPath/Division_Main.sas";

%include "&ProgPath/Evaluation_Disposition.sas";
*%include "&ProgPath/Evaluation_Withdrawals.sas";
*%include "&ProgPath/Evaluation_Deviations.sas";
%include "&ProgPath/Evaluation_Demography.sas";

%include "&ProgPath/Evaluation_VS_Num.sas";
%include "&ProgPath/Evaluation_VS_Cat.sas";
%include "&ProgPath/Evaluation_PE.sas";

%include "&ProgPath/Evaluation_MD.sas";
*%include "&ProgPath/Evaluation_MH.sas";
%include "&ProgPath/Evaluation_CM.sas";
%include "&ProgPath/Evaluation_Comp.sas";

%include "&ProgPath/Evaluation_ExpObj_Symp.sas";
%include "&ProgPath/Evaluation_ExpObj_Time.sas";

*%include "&ProgPath/Evaluation_SafObj.sas";
*%include "&ProgPath/Evaluation_AE_Common.sas";
*%include "&ProgPath/Evaluation_AE_DrugSevDistr.sas";

%include "&ProgPath/Generation_EmptyFiles.sas";

%include "&ProgPath/Listings.sas";
