												/* Division_Main.sas */
								/* Formation of Analysed Datasets from Project Export */

%let Intxt0     =  6301_Rand;								/* Input data in Text format for Randomization */
%let Intxt1     =  6301_Main;								/* Input data in Text format for Main Parameters */
%let Intxt2     =  6301_MH;								/* Input data in Text format for Medical History */
%let Intxt3     =  6301_CM;								/* Input data in Text format for Concominant Medication */
*%let Intxt4    =  6302_Dev;								/* Input data in Text format for Protocol Deviations */

%let Input0     =  Data_Export_Rand;					/* Input data as SAS Dataset for Randomization */
%let Input1     =  Data_Export_Main;					/* Input data as SAS Dataset for Main Parameters */
%let Input2     =  Data_Export_MH;						/* Input data as SAS Dataset for Medical History */
%let Input3     =  Data_Export_CM;						/* Input data as SAS Dataset for Concominant Medication */

%let Popul	    =  PopulFlags;							/* Separate File for Patients Statuses,      */
																	/* Randomization Groups and Population Flags */

%let Output1    =  Screening;								/* Output data for Screening */
%let Output2    =  Randomization;						/* Output data for Randomization */
%let Output3    =  Data_Withdrawals;					/* Output data for Withdrawals */
%let Output4    =  ADSL;									/* Output data for ADSL */

%let Output5_1  =  Data_VS;								/* Output data for Vital Signs */
%let Output5_2  =  Data_PE;								/* Output data for Physical Examination */

* %let Output5_BP =  Data_VS_BP;							/* Output data for Vital Signs, Blood Pressure */
* %let Output5_VS =  Data_VS_Main;						/* Output data for Vital Signs, Other Parameters */
* %let Output5_PE =  Data_PE;								/* Output data for Physical Examination */

%let Output6    =  Data_MD;								/* Output data for Medical Diagnosis (Anamnesis) */
%let Output7    =  Data_MH;								/* Output data for Medical History */
%let Output8    =  Data_CM;								/* Output data for Concominant Medication */
%let Output9    =  Data_Comp;								/* Output data for Compliance */
%let Output10   =  Data_Symp;								/* Output data for Symptoms */

*%let OutputX   =  Data_Obj_Main;						/* Output data for Main Exploratory Objectives */

%let OutFile    =  Data_Main;								/* Output File Name */

																	/* Delite All Files from Folder Library */
/* proc datasets	lib		= Libr */
/* 					memtype	= data */
/* 					kill */
/* 					nolist; */
/* quit; */
																	/* Delite Temporary Files from Working Library */
proc datasets	lib		= Work
					memtype	= data
					kill
					nolist;
quit;
														/* Formation of a Dataset for Randomization from Text Import */
proc import	file = "&InPath./&Intxt0..txt"
				out  = Libr.&Input0.
				replace ;
				datarow = 3;
				guessingrows = 1000;
run;
														/* Formation of a Dataset for Main Parameters from Text Import */
proc import	file = "&InPath./&Intxt1..txt"
				out  = Libr.&Input1.
				replace ;
				datarow = 3;
				guessingrows = 1000;
run;
														/* Formation of a Dataset for Medical History from Text Import */
proc import	file = "&InPath./&Intxt2..txt"
				out  = Libr.&Input2.
				replace ;
				datarow = 3;
				guessingrows = 1000;
run;
														/* Formation of a Dataset for Concominant Medication from Text Import */
proc import	file = "&InPath./&Intxt3..txt"
				out  = Libr.&Input3.
				replace ;
				datarow = 3;
				guessingrows = 1000;
run;
											/* Adding Randomization Groups to and Create Population Flags in the Main Dataset */

*%let PopFlags	= FASFL  PPFL;				/* Populations Flags: FAS/PerProtocol */

*proc sort	data = Libr.&Input0.;
*				by	  RandNum;
*run;
proc sort	data = Libr.&Input1.;
				by   RandNum;
run;
data			Libr.&Input1.;
	format	Screen_Num  Status
				RandNum  Group  Group_Name
				_ALL_;
	merge		Libr.&Input0.
				Libr.&Input1.;
	by			RandNum; /* ! */

	array		PopFlag[&NP] &PopFlags;
	drop		n;
												/* Assigning Population Flags on the base of IP Acceptance and Status of Study Ending */
			do n = 1 to &NP;
				PopFlag[n] = 0;
			end;
		/* IP Acceptance Flag */
			if(DVSTAT  = "Y") then
				FASFl   =  1;
		/* Status of Study Ending */			
			if(DSDECOD =  2) then
				PPFl    =  1;
run;
proc sort	data = Libr.&Input1.;
	by			Screen_Num;
	where		Screen_Num ^= "";
*	where		Screen_Num is not missing;
run;
														/* Formation of Separate Dataset for Status, Groups and Population Flags */
data		Libr.&Popul.;
	set	Libr.&Input1.;
	keep	Screen_Num  Status  Group  &PopFlags;
run;
														/* Formation of a Dataset for Screening */

*%let InVarScr	=	Site_Num  SVSTDAT  RFICDTC
						IEYN  IETESTCD_I  IETESTCD_E  IEORRES_EC
						Status
						;
%let ReNamScr	=	Site_Num		=	Site_Num
						SVSTDAT		=	ScrDT
						RFICDTC		=	ICFDT
						IEYN			=  CritFl
						IETESTCD_I	=	InCritX
						IETESTCD_E	=	ExCritX
						IEORRES_EC1	=  CritConf
						Status		=	Status	/* SUBJSTT */
						;
%let OutVarScr	=	Site_Num  ScrDT  ICFDT
						CritFl  InCritX  ExCritX  CritConf
						Status
						;
														/* Formation of a Dataset for Screening */
data			Libr.&Output1.;
	format	Screen_Num  &OutVarScr  &PopFlags;
	keep		Screen_Num  &OutVarScr  &PopFlags;
	set		Libr.&Input1.
  (rename= (&ReNamScr));
run;
														/* Formation of a Dataset for Randomization */

*%let InVarRand =	Site_Num  IEORRES_EC  IE_COM
						RANDNUM   DAORRES     DAREASND
						;
%let ReNamRand	 =	Site_Num		=	Site_Num
						IEORRES_EC1	=  CritConf
						IE_COM		=	ReasXRand
						RANDNUM		=	RandNum
						;
%let OutVarRand =	Site_Num  CritConf
						ReasXRand  RandNum   Group
						;
														/* Formation of a Dataset for Randomization */
data			Libr.&Output2.;
	format	Screen_Num  &OutVarRand;
	keep		Screen_Num  &OutVarRand;
	set		Libr.&Input1.
  (rename= (&ReNamRand));
run;

									/* Number of Withdrawal Reasons, */
%let NWD = 4;					/* their Initial and Final Names */

%let InVarWD	=	DSTERM_A_COMM  DSTERM_B_COMM  DSTERM_C_COMM  DSTERM_H_COMM
						;	/* Discontinuation Termination Reasons */
%let OutArrWD	=	DctReas
						;	/* Discontinuation Reason */
%let ReNameWD	=	DSDECOD        = StudyEnd
						DSTERM_OTHTEXT = DctReasOth
						;	/* Other Discontinuation Reason */
														/* Formation of a Dataset for Withdrawals */
data			Libr.&Output3.;
	format	Screen_Num  Group
				StudyEnd
				&OutArrWD.1-&OutArrWD.&NWD
				DctReasOth
				&PopFlags;
	keep		Screen_Num  Group
				StudyEnd
				&OutArrWD.1-&OutArrWD.&NWD
				DctReasOth
				&PopFlags;
	set		Libr.&Input1.
  (rename= (&ReNameWD));
  
	array		InArrWD[&NWD]  &InVarWD;
	array		&OutArrWD[&NWD];
	
	do			n = 1 to &NWD;
		select (InArrWD[n]);
			when ("True")  do;	&OutArrWD[n] = 1;	end;
			when ("False") do;	&OutArrWD[n] = 0;	end;
			otherwise		do;	&OutArrWD[n] = .;	end;
		end;
	end;
run;

																		/* Initial ADSL Variables Names */
*%let InVarADSL =	SEX	RACE	RACE_OTH
						SVSTDAT  BRTHDAT
						;
%let ReNameADSL =	SEX  = InSex
						RACE = InRace
						;
%let OutVarADSL =	Sex  Race  Race_Oth  Age
						;
%let DaysInYear = 365.25;
														/* Formation of a Dataset for Disposition (ADSL) */
data			Libr.&Output4.;
	format	Screen_Num   Group
				&OutVarADSL  &PopFlags;
	keep		Screen_Num   Group
				&OutVarADSL  &PopFlags;
	set		Libr.&Input1.
  (rename= (&ReNameADSL));
				
				select (InSex);
					when ("M") do;  Sex = 1;  end;
					when ("F") do;  Sex = 2;  end;
					otherwise  do;  Sex = .;  end;
				end;
				
				select (InRace);
					when  ("1")  do;  Race = 1;  end;
					when ("OTH") do;  Race = 2;  end;
					otherwise    do;  Race = .;  end;
				end;

				Age = (SVSTDAT - BRTHDAT)/&DaysInYear.;
run;

										/* Vital Signs */

%let NVS     = 4;				/* Number of Vital Signs */
%let NVSType = 3;				/* Number of Vital Signs Types */

									/* Vital Signs Parameters */

*%let VSPar0 = BPS;			/* Blood Pressure Systolic  */
*%let VSPar1 = BPD;			/* Blood Pressure Diastolic */

%let VSPar1 = BP;				/* Blood Pressure */
%let VSPar2 = HR;				/* Heart Reductions */
%let VSPar3 = RR;				/* Respiratory Reductions */
%let VSPar4 = TEMP;			/* Temperature */

										/* Vital Signs Types */

%let VSType1 = ORRES;		/* Operation Result */
%let VSType2 = NRIND;		/* Normality Identification */
%let VSType3 = CLSIG;		/* Clinical Significance (of Deviation) */

															/* Designing String (VarVS) with Vital Signs Variables Names */
proc sql noprint;
	select trim(compress(name))
	into :VarVS separated by " "
	from dictionary.columns
	where libname = upcase("Libr")
	and memname = upcase("&Input1.")
	and upcase(name) like "%VS%"
	and upcase(name) not like "%SVS%";
quit;

%put &=VarVS;
															  /* Formation of a Datasets for Vital Signs */
data			Libr.&Output5_1.;
	format	Screen_Num  Group
				&VarVS  &PopFlags;
	keep		Screen_Num  Group
				&VarVS  &PopFlags;
	set		Libr.&Input1.;
run;

											/* Physical Examination */

%let NPE     = 11;				/* Number of Physical Examination */
%let NPEType =  3;				/* Number of Physical Examination Types */

%let PEPar1  =  APPEAR;
%let PEPar2  =  SKIN  ;
%let PEPar3  =  LOR   ;
%let PEPar4  =  EYE   ;
%let PEPar5  =  RESP  ;
%let PEPar6  =  CV    ;
%let PEPar7  =  GASTR ;
%let PEPar8  =  MOTOR ;
%let PEPar9  =  ENDOCR;
%let PEPar10 =  NEURO ;
%let PEPar11 =  REN   ;
										/* Physical Examination Types */
%let PEType1 = NRIND;			/* Normality Identification */
%let PEType2 = CLSIG;			/* Clinical Significance (of Deviation) */
%let PEType3 = ORRES;			/* Operation Result (Description) */

															/* Designing String (VarPE) with Physical Examination Variables Names */
proc sql noprint;
	select trim(compress(name))
	into :VarPE separated by " "
	from dictionary.columns
	where libname = upcase("Libr")
	and memname = upcase("&Input1.")
	and upcase(name) like "%PE%";
quit;

%put &=VarPE;
															  /* Formation of a Datasets for Physical Examination */
data			Libr.&Output5_2.;
	format	Screen_Num  Group
				&VarPE  &PopFlags;
	keep		Screen_Num  Group
				&VarPE  &PopFlags;
	set		Libr.&Input1.;
run;

														/* Formation of a Dataset for Medical Diagnosis (Anamnesis) */
%let ReNameMD  =	MHSEV_MD = SevMD
						;
%let OutVarMD  =	DurMD  SevMD
						;
%let HoursInDay   =  24;
%let SecondsInDay = (24*60*60);

data			Libr.&Output6.;
	format	Screen_Num   Group
				&OutVarMD  &PopFlags;
	keep		Screen_Num   Group
				&OutVarMD  &PopFlags;
	set		Libr.&Input1.
  (rename= (&ReNameMD));

				DurMD = ((SVSTDAT - MHSTDAT_MD) +
							(SVSTTIM - MHSTTIM_MD) / &SecondsInDay.)
						  								  *  &HoursInDay.  ;
run;

															  /* Formation of a Datasets for Medical History */
data			Libr.&Output7.;
	set		Libr.&Input2.;
	keep		Screen_Num
				MHTerm   MHENRF;
	rename	MHENRF = ContFl;
				MHTerm = lowcase (MHTerm);
run;
																	/* Sort for Merging */
proc	sort
	data	=	Libr.&Output7.;
	by			Screen_Num;

data			Libr.&Output7.;
	merge		Libr.&Output7.
				Libr.&Popul.;
*	if			MHTerm ^= "";	/* ! if -- not where ! */
	by			Screen_Num;		/* ! by ! */
	drop		Status;
run;


%let VarCM		=	CMTRT  CMINDC											/* CM Treatment, Indications */
						CMSTDAT_DAY  CMSTDAT_MONTH  CMSTDAT_YEAR		/* Start Day, Month, Year    */
						CMENDAT_DAY  CMENDAT_MONTH  CMENDAT_YEAR		/* End   Day, Month, Year    */
						CMONG;													/* Continuation Flag         */
															  /* Formation of a Datasets for Concominant Medication */
data			Libr.&Output8.;
	set		Libr.&Input3.;
	keep		Screen_Num
				&VarCM;
				CMTrt = lowcase (CMTrt);
run;
																	/* Sort for Merging */
proc	sort
	data	=	Libr.&Output8.;
	by			Screen_Num;

data			Libr.&Output8.;
	merge		Libr.&Output8.
				Libr.&Popul.;
*	if			CMTrt ^= "";	/* ! if -- not where ! */
	by			Screen_Num;		/* ! by ! */
	drop		Status;
run;

														/* Formation of a Dataset for Medical Diagnosis (Anamnesis) */
%let ReNameComp  =	V4_DAORRES_COMP = Comp
							;
%let OutVarComp  =	Comp
							;

data			Libr.&Output9.;
	format	Screen_Num   Group
				&OutVarComp  &PopFlags;
	keep		Screen_Num   Group
				&OutVarComp  &PopFlags;
	set		Libr.&Input1.
  (rename= (&ReNameComp));
run;

											/* Symptoms Ranging */

%let NSymp    =  13;						/* Number of Symptoms */
%let Symp     =  QSORRES_Q;			/* Base Code for Symptoms */
%let SympTot  =  QSORRES_TOTAL;		/* Base Code for Symptoms Totals */
%let VisDate  =  SVSTDTC;				/* Base Code for Visits Dates */
%let SyStDate =  MHSTDAT_MD;			/* Code for Symptoms Starting Date */

															/* Designing String (SympList) with Symptoms List  */
proc sql noprint;
	select trim(compress(name))
	into :SympList separated by " "
	from dictionary.columns
	where libname = upcase("Libr")
	and memname = upcase("&Input1.")
	and upcase(name) like "%QS%";
quit;

*%put &=SympList;
															/* Designing String (VisDates) with Visits Dates  */
proc sql noprint;
	select trim(compress(name))
	into :VisDates separated by " "
	from dictionary.columns
	where libname = upcase("Libr")
	and memname = upcase("&Input1.")
	and upcase(name) like "%SVSTDTC%";
quit;

															  /* Formation of a Datasets for Physical Examination */
data			Libr.&Output10.;
	format	Screen_Num  Group
				&SympList   &SyStDate  &VisDates
				&PopFlags;
	keep		Screen_Num  Group
				&SympList   &SyStDate  &VisDates
				&PopFlags;
	set		Libr.&Input1.;
run;

															/* Printing All Outputs in Specified Format */
%macro OutputPrint (frm);
	ods &frm path		= "&OutPath"
				file		= "&OutFile..&frm";
	
	title	"Output data for Screening";
	proc print data = Libr.&Output1.;
	run;
	title	"Output data for Randomization";
	proc print data = Libr.&Output2.;
	run;
	title	"Output data for Withdrawals";
	proc print data = Libr.&Output3.;
	run;
	title	"Output data for ADSL";
	proc print data = Libr.&Output4.;
	run;
	title	"Output data for Vital Signs";
	proc print data = Libr.&Output5_1.;
	run;
	title	"Output data for Physical Examination";
	proc print data = Libr.&Output5_2.;
	run;
	title "Output data for Medical Diagnosis";
	proc print data = Libr.&Output6.;
	run;
	title "Output data for Medical History";
	proc print data = Libr.&Output7.;
	run;
	title "Output data for Concominant Medication";
	proc print data = Libr.&Output8.;
	run;
	title "Output data for Compliance";
	proc print data = Libr.&Output9.;
	run;
	title "Output data for Exploratory Objectives";
	proc print data = Libr.&Output10.;
	run;
%*	title "Output data for Laboratory";
%*	proc print data = Libr.&OutputXX.;
%*	run;

	ods &frm close;
%mend;

	%OutputPrint (rtf);							/* Creating rtf-output */
