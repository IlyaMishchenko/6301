﻿																/* Evaluation_CM.sas */
												 /* Formation of Concominant Medication Table */

%let PopFl  =  FASFL;								/* FAS Population */
%let PN     =  1;										/* FAS Population Number */

%let Input  =  Data_CM;								/* Input data for Concominant Medication */

%let Total	=  Totals_Pop;							/* Population Totals */
%let Output =  CM;									/* Name Basis for Output data indexed by Populations (_&&Post&j) */

															/* Output File Name */
%let OutFile =  Приложение_9_Предшествующая_и_сопутствующая_терапия;

%let Summ	=  Summary;								/* Temporary file for Summary Statistics for Concominant Medication */
%let SumT	=  TextSumm;							/* Temporary file for Summary Statistics for Concominant Medication as Text */

%let Title = Таблица 10.9. Предшествующая и сопутствующая терапия. Популяция всех включенных пациентов;
%let FootNote = Проценты указаны относительно количества пациентов в соответствующей группе;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */


%macro CMTables ();
																		/* Initiation of Summary Text File for First Merging */
																		/* by Full List of Symptoms (for All Groups) */
		proc sort	data   = Libr.&Input.
					  (rename = (CMTrt = Label))
						out    = Work.&SumT.
					  (keep   =	 Label)
						nodupkey;
			by  		Label;
			where		&PopFl
				and	Label ^= "";
		run;

	%do g = 1 %to &NG.;
																		/* Preventing Missings in Output File*/
		data			Work.&SumT.;
			set		Work.&SumT.;
						Count = 0;
		run;
																		/* Summarizing Concominant Medication over Patients for Current Group */
		proc summary	data   = Libr.&Input.;
				where		&PopFl
					and	Group  = &g.;
				output	out    = Work.&Summ.
						  (drop   = _Type_
							rename =
						  (CMTrt = Label
							_Freq_ = Count));
				ways		1;	
				class		CMTrt;	/* Not Sorted Data */
%*				by			CMTrt;		 /* Sorted Data */
		run;
																		/* Merging Current Summary File With Common File */
																		/* and Rewriting Frequencies as Text Column */
		data			Work.&SumT.;
		  	merge		Work.&SumT.
						Work.&Summ.;
			by			Label;

%*			length	Number&g. $20;
			keep		Label
						Number1 - Number&NG.;

			if	(&&TotP&PN.&g. ^= 0) then
				do;
						Percent   = Count/&&TotP&PN.&g.*100;
						Number&g. = strip(put(Count,   CommaX5.0)) ||
							 " (" || strip(put(Percent, CommaX5.1)) || "%)";
				end;
			else		Number&g. = "0";
		run;
	%end;

		data			Libr.&Output.;
			length	Name $200
						Number1 - Number&NG. $20;
			keep		Name
						Number1 - Number&NG.;
			set		Work.&SumT.;

						Name  = Label;
%*						Name  = "&Tab" || Label;
		run;
%mend;

		%CMTables ();
																							/* Printing Output Table in rtf-format */
options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data;

ods rtf path = "&OutPath"
		  body = "&OutFile..rtf";

ods rtf text = "#S={just=j}&Title." startpage=now;  
ods rtf text = "#S={fontsize=6pt}";  

%macro GroupsReport1 ();
	proc report	data = Libr.&Output.
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "";
	 %do g = 1 %to &NG.;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
%mend;
		 %GroupsReport1 ();

ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  

ods rtf close;
