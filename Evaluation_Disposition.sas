													/* Evaluation_Disposition.sas */
												/* Formation of Disposition Table */

*%let g = 0;										/* Index of Group (0 = Total) */

%let DN = 1;										/* Disposition Number for Randomized Status */

%let Input   =  PopulFlags;					/* Input  data */
%let Output  =  Disposition;					/* Output data */

														/* Output File Name */
%let OutFile =  Приложение_1_Распределение_пациентов;

%let Totals1 =  Totals_Dis;					/* Totals for Disposition */
%let Totals2 =  Totals_Pop;					/* Totals for Population  */

*%let Temp	 =  Temporary;						/* Universal Temporary Summary file */

%let TempD	 =  TemporaryD;					/* Disposition Partial Summary file */
%let TempDT	 =  TemporaryDT;					/* Disposition Partial Transposed Summary file */

%let TempP	 =  TemporaryP;					/* Population  Temporary Summary file */
%let TempPT	 =  TemporaryPT;					/* Population  Temporary Summary file */

%let Temp1	 =  Temporary1;					/* Temporary file for Disposition Header */
%let Temp2	 =  Temporary2;					/* Temporary file for Disposition Body   */
%let Temp3	 =  Temporary3;					/* Temporary file for Population  Header */
%let Temp4	 =  Temporary4;					/* Temporary file for Population  Body   */
*%let Temp5	 =  Temporary5;					/* Temporary file for Table Footnotes    */

%let Note1   = "Статус пациента";			/* Title Note for Disposition */
%let Note2   = "Анализируемые популяции";	/* Title Note for Population  */
														/* Title for Table */
%let Title   = Таблица 10.1. Распределение пациентов;
%let FootNote= Проценты указаны относительно количества рандомизированных пациентов в соответствующей группе;
														/* Footnote for Table         */
%let Status1 = Рандомизирован;
%let Status2 = Выбыл досрочно;
%let Status3 = Завершил по окончании всех запланированных визитов;

proc format;												/* Format for Disposition Status Names */
 value $ DisFmt
  "TotD1" = "     &Status1"
  "TotD2" = "     &Status2"
  "TotD3" = "     &Status3"
  																	/* Format for Population Names */
  "TotP1" = "     Популяция всех включенных пациентов"
  "TotP2" = "     Популяция оценки клинического эффекта"
  ;
													/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */
&TitleSAR;	&FootnoteSAR;
																			/** Formation of Output for Disposition **/
																		/* Distribution of Patients over Disposition Status */
%macro DisTotals ();

	%do g = 0 %to &NG;
	
		data			Work.&TempD.;
				set	Libr.&Input.
				end = Nomore;
				keep	TotD1-TotD&ND.;
				
			if (&g. = 0 | Group = &g.) then do;
				select  (  Status );
					when ("&Status1") do;	TotD1 + 1;	end;
					when ("&Status2") do;	TotD2 + 1;	end;
					when ("&Status3") do;	TotD1 + 1;  TotD3 + 1;	end;
					otherwise;
				end;
			end;

			if (Nomore) then  output;
			
%*			if (Nomore) then do;
%*					output;
%*																		/* Assigning MacroVariable Norm&g. for Randomized Total */
%*					call	symput ( "Norm&g.", TotD&DN. );
%*					call	symput ( "NN&g."  , "N = " || compress(put(TotD&DN., 4.)) );		
%*																		/* Saving Randomized Total as Text String */
%*				end;
		run;
																		/* Transposing Status Numbers to a Single Column */
		proc transpose	data = Work.&TempD.
							out  = Work.&TempDT.
							name   = Name
							prefix = Tot;
		run;
																		/* Merging Status Columns for All Groups Together */
		data			Libr.&Totals1.;
			%if (&g ^= 0) %then %do;
				set	Libr.&Totals1.;
			%end;
				set	Work.&TempDT.
					 ( rename =
					  (Tot1 = Tot&g.));
		run;

	%end;
%mend;

			%DisTotals();
															/** Reading Totals for All Disposition Statuses by Groups **/
															/** and Saving them as Numeric and Text MacroVariables    **/
%macro DisTotalsRead ();
	%do d = 1 %to &ND.;
		%do g = 0 %to &NG.;
			%global	TotD&d.&g.  ND&d.&g.;
		%end;
	%end;

	data 			_null_;
		set		Libr.&Totals1.;
			%do d = 1 %to &ND.;
				if (_N_ = &d.) then do;
					%do g = 0 %to &NG.;
						call	symput ( "TotD&d.&g.", Tot&g. );
						call	symput (   "ND&d.&g.", "N = " ||
											 compress(put(Tot&g., 4.)) );		
%put  &=TotD&d.&g.;
					%end;
				end;
			%end;
	run;
%put  &=TotD10;
%mend;

		%DisTotalsRead ();

%put  &=TotD11  &=TotD12  &=TotD10;
%put  &=TotD21  &=TotD22  &=TotD20;
%put  &=TotD31  &=TotD32  &=TotD30;
%put    &=ND11    &=ND12    &=ND10;
%put    &=ND21    &=ND22    &=ND20;
%put    &=ND31    &=ND32    &=ND30;


%let PopTots = TotP1  TotP2;								/* Totals for Patients Populations: FAS  PerProtocol */
																			/** Formation of Totals for Populations **/
%macro PopTotals ();

	%do g = 0 %to &NG;
																		/* Summarizing Population Totals over Patients */
		proc summary	data = Libr.&Input.;
				where		&g.  = 0 or Group = &g.;
				output	out  = Work.&TempP.
						  (drop = _Type_  _Freq_)
							sum   (&PopFlags)
								  = &PopTots;
		run;
																		/* Transposing Population Numbers to a single Column */
		proc transpose	data = Work.&TempP.
							out  = Work.&TempPT.
							name   = Name
							prefix = Tot;
		run;
																		/* Merging Status Columns for All Groups Together */
		data			Libr.&Totals2.;
			%if (&g ^= 0) %then %do;
				set	Libr.&Totals2.;
			%end;
				set	Work.&TempPT.
					 ( rename =
					  (Tot1 = Tot&g.));
		run;

	%end;
%mend;

			%PopTotals();
																/** Reading Totals for All Populations by Groups **/
															/** and Saving them as Numeric and Text MacroVariables **/
%macro PopTotalsRead ();
	%do p = 1 %to &NP.;
		%do g = 0 %to &NG.;
			%global	TotP&p.&g.  NP&p.&g.;
		%end;
	%end;

	data 			_null_;
		set		Libr.&Totals2.;
			%do p = 1 %to &NP.;
				if (_N_ = &p.) then do;
					%do g = 0 %to &NG.;
						call	symput ( "TotP&p.&g.", Tot&g. );
						call	symput (   "NP&p.&g.", "N = " ||
											 compress(put(Tot&g., 4.)) );		
					%end;
				end;
			%end;
	run;
%mend;

		%PopTotalsRead ();

%put  &=TotP11  &=TotP12  &=TotP10;
%put  &=TotP21  &=TotP22  &=TotP20;
%put    &=NP11    &=NP12    &=NP10;
%put    &=NP21    &=NP22    &=NP20;
/*
proc sql noprint;
	select Tot0, Tot1, Tot2
	into  :TotP01 - :TotP02,
			:TotP11 - :TotP12,
			:TotP21 - :TotP22
	from   Libr.&Totals.;
quit;

%put &=TotP01  &=TotP11  &=TotP21;
%put &=TotP02  &=TotP12  &=TotP22;

%macro PopTotalsRead2 ();
	proc sql noprint;
		%do g = 0 %to &NG.;
			select Tot&g.
			into  :TotP&g.1 - :TotP&g.&NP.,
			from   Libr.&Totals.;
		%end;
	quit;
%mend;
		%PopTotalsRead2 ();

%put &=TotP01  &=TotP11  &=TotP21;
%put &=TotP02  &=TotP12  &=TotP22;
*/
																/** Formation of Output Table for Disposition and Population **/
%macro DisPopTable ();
																		/* Creating Header Lines for Disposition */
	data			Work.&Temp1.;
		length	Name $200
					Number0 - Number&NG. $20;
					Name = &Note1;
	run;
																		/* Creating Main Body for Disposition */
	data			Work.&Temp2.;
		length	Name  $200
					Number0 - Number&NG. $20;
		keep		Name
					Number0 - Number&NG.;
		set		Libr.&Totals1.;
			
			%do g = 0 %to &NG;
				if (_N_ <= &DN.) then do;
					if (Tot&g. = .) then						/* Omitting Nonsencial Values */
							Number&g. = "";
					else
							Number&g. = strip(put(Tot&g.,  CommaX5.0));
				end;
				else do;
							Percent   = Tot&g./&&TotD&DN.&g.*100;
							Number&g. = strip(put(Tot&g.,  CommaX5.0)) ||
								 " (" || strip(put(Percent, CommaX5.1)) || "%)";
					 end;
			%end;
	run;
																		/* Creating Header Lines for Population */
	data			Work.&Temp3.;
		length	Name  $200
					Number0 - Number&NG. $20;
					Name  = &Note2;
	run;
																		/* Creating Main Body for Population  */
	data			Work.&Temp4.;
		length	Name $200
					Number0 - Number&NG. $20;
		keep		Name
					Number0 - Number&NG.;
		set		Libr.&Totals2.;
	
			%do g = 0 %to &NG;
					Percent   = Tot&g./&&TotD&DN.&g.*100;
					Number&g. = strip(put(Tot&g.,  CommaX5.0)) ||
						 " (" || strip(put(Percent, CommaX5.1)) || "%)";		
			%end;
	run;
																			/* Vertical Concatination of Table Parts */
	data			Libr.&Output;
		format	Name $DisFmt.
					Number1 - Number&NG.
					Number0;
		set		Work.&Temp1
					Work.&Temp2
					Work.&Temp3
					Work.&Temp4;
	run;

%mend;

			%DisPopTable();

options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf file="&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data;

																/* Printing Output for Disposition as rtf-output */
ods rtf path = "&OutPath"
		  body = "&OutFile..rtf";

ods rtf text="#S={just=j}&Title." startpage=now;  
ods rtf text="#S={fontsize=6pt}";  

%macro GroupsReport ();
	proc report	data = Libr.&Output.
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=54%} "";
	 %do g = 1 %to &NG;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&ND&DN.&g.";
	 %end;
		define Number0   / display style(column)={asis=on just=c width=15%} "&GName0|&&ND&DN.0";
	run;
%mend;
		 %GroupsReport ();

ods rtf text="#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  

ods rtf close;
