													/* Evaluation_ExpObj_Symp.sas */
									/* Evaluation of Exploratory Objectives for Symptoms */

%let PopFl  =  PPFL;									/* Per Protocol Population */
%let PN     =  2;										/* Per Protocol Population Number */

%let Input  =  Data_Symp;							/* Input data */
%let Analys =  Analyzed_Symp;						/* Analysed data */
%let Output =  ExpObj_Symp;						/* Name for Output data */

															/* Output File Name */
%let OutFile=  Приложение_12_ИКТ_Симптомы_заболевания;

%let Templ  =  Template;							/* Name for Template Dataset */
%let Summ   =  Summary;								/* Name for Family of Working Summary Files */

%let NExOb  =  4;										/* Number of Analysing Exploratory Objectives (&k) */

*%let NSymp = 13;										/* Number of Symptoms (&j) */
*%let NV    =  4;										/* Number of Visits   (&i) */
*%let NG    =  2;										/* Number of Groups   (&g) */

															/* Head  Notes for Symptoms */
%let Head1  = "Чихание";
%let Head2  = "Ринорея";
%let Head3  = "Заложенность носа";
%let Head4  = "Кашель";
%let Head5  = "Лихорадка";
%let Head6  = "Недомогание";
%let Head7  = "Озноб";
%let Head8  = "Головная боль";
%let Head9  = "Миалгия";
%let Head10 = "Боль в горле";
%let Head11 = "Першение";
%let Head12 = "Охриплость";
%let Head13 = "Склерит/конъюнктивит";
															/* Title Notes for Visits */
%let Note1  = "&Tab.День 1";
%let Note2  = "&Tab.День 3";
%let Note3  = "&Tab.День 6";
%let Note4  = "&Tab.День 14";
															/* Titles for Exploratory Objectives */

%let Title1 =  Таблица 11.1. 1-я поисковая конечная точка.
Доля пациентов с наличием симптома ОРВИ или гриппа (с оценкой симптома 2 или 3).
Популяция оценки клинического эффекта;
%let Title2 =  Таблица 11.2. 2-я поисковая конечная точка.
Доля пациентов с наличием симптома ОРВИ или гриппа средней степени выраженности (с оценкой симптома 2).
Популяция оценки клинического эффекта;
%let Title3 =  Таблица 11.3. 3-я поисковая конечная точка.
Доля пациентов с наличием симптома ОРВИ или гриппа тяжелой степени выраженности (с оценкой симптома 3).
Популяция оценки клинического эффекта;
%let Title4 =  Таблица 11.4. 4-я поисковая конечная точка.
Доля пациентов с отсутствием симптома ОРВИ или гриппа (с оценкой симптома 1).
Популяция оценки клинического эффекта;
															/* Titles for Exploratory Objectives Tables */
%let TabTitle1 =  Оценка симптома 2 или 3;
%let TabTitle2 =  Оценка симптома 2;
%let TabTitle3 =  Оценка симптома 3;
%let TabTitle4 =  Оценка симптома 1;

%let FootNote = Проценты указаны относительно количества пациентов с доступными данными
в соответствующей группе на соответствующей стадии лечения;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */

													/* Formation of Analysed Datasets for Exploratory Objectives */
%macro AnalysedDatasets ();
																			/* Cleaning and Sorting Initial Dataset */
	proc sort	data = Libr.&Input.
					out  = Work.&Analys.;
		where		&PopFl;
		by			Group;
	run;
													/* Evaluation of Analysed Variables for Each Exploratory Objective */
	data	%do	k = 1 %to &NExOb.;
					Work.&Analys.&k.
			%end;
					;
		set		Work.&Analys.;
		keep		Group
			 %do  j = 1 %to &NSymp.;
					Symp&j.V1 - Symp&j.V&NV.
			 %end;
					;
%*		where		&PopFl = 1;
																	/* Saving Analysing Variables */
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (2, 3)  Symp&j.V&i. = 1;
				 when  (1)     Symp&j.V&i. = 0;
				 otherwise     Symp&j.V&i. = .;
				end;
			  %end; %end;
					output Work.&Analys.1;
			  
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (2)     Symp&j.V&i. = 1;
				 when  (1, 3)  Symp&j.V&i. = 0;
				 otherwise     Symp&j.V&i. = .;
				end;
			  %end; %end;
					output Work.&Analys.2;
			  
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (3)     Symp&j.V&i. = 1;
				 when  (1, 2)  Symp&j.V&i. = 0;
				 otherwise     Symp&j.V&i. = .;
				end;
			  %end; %end;
					output Work.&Analys.3;
			  
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (1)     Symp&j.V&i. = 1;
				 when  (2, 3)  Symp&j.V&i. = 0;
				 otherwise     Symp&j.V&i. = .;
				end;
			  %end; %end;
					output Work.&Analys.4;
	run;
%mend;
*			%AnalysedDatasets ();

															/* Creating Macrovariables of Formating Symptoms Index (01 - 13) */
%macro IndexFormat ();
	 %do j = 1 %to &NSymp.;
			%global  jj&j.;
	 %end;

	data	_null_;
	 %do j = 1 %to &NSymp.;
			call symput ("jj&j.", put(&j.,z2.));
	 %end;
	run;	/* ! */
%mend;
*			%IndexFormat();

																		/* Creating Template for Exploratory Objective Tables */
%macro ExObTableTemplate ();

	data			Work.&Templ.;
		length	_Name_ $20
					 Name  $200;
	
		 %do  j = 1 %to &NSymp.;
		 
		  			_Name_ = "Symp&&jj&j..Vis0";
					 Name  = &&Head&j.;
						output;
						
		  %do i = 1 %to &NV.;
		  			_Name_ = "Symp&&jj&j..Vis&i.";
		  			 Name  = &&Note&i.;
		  				output;
		  %end;
				if (&j. < &NSymp.) then do;
		  			_Name_ = "Symp&&jj&j..Vis&i.";
					 Name  = ""; 	/* &NV.+ 1 */
						output;
			 	end;
		 %end;
	run;
%mend;
*			%ExObTableTemplate ();

																			/* Summary Statistics for Exploratory Objectives */
%macro ExObStatSumm (k); /* Exploratory Objective Number */

	proc summary	data = Work.&Analys.&k.;
			output	out  = Work.&Summ.
						n 		(_All_) =
						sum 	(_All_) = 	
					 / autoname
						;
			by			Group;
	run;
																		/* Evaluating Percentages and Saving Them as Text */
	data				Work.&Summ.P;
			set		Work.&Summ.;
			keep
				 %do	j = 1 %to &NSymp.;
						Symp&&jj&j..Vis1 - Symp&&jj&j..Vis&NV.
				 %end;
						;
			length	
				 %do	j = 1 %to &NSymp.;
						Symp&&jj&j..Vis1 - Symp&&jj&j..Vis&NV.
				 %end;
						$20;
		
				 %do  j = 1 %to &NSymp.;
				  %do i = 1 %to &NV.;
						Friqency =	Symp&j.V&i._Sum ;
						Percent  =	Symp&j.V&i._Sum /
										Symp&j.V&i._N   * 100;
						
						Symp&&jj&j..Vis&i. = strip(put(Friqency, CommaX5.0)) ||
										 " (" || strip(put(Percent , CommaX5.0)) || "%)";
				  %end; %end;
	run;
																		/* Transposing Text Statistics from Wide to Long Representation */
	proc transpose	data = Work.&Summ.P
						out  = Work.&Summ.T;
						var  _All_;
	run;
																		/* Merging Text Statistic with the Template */
	data				Libr.&Output.&k.;
			merge		Work.&Templ.
						Work.&Summ.T;
			by 		_Name_;
			drop		_Name_;
	run;
%mend;

													/* Printing Results for Specified Exploratory Objective */
%macro ExObReport (k);

	ods rtf text = "#S={just=j}&&Title&k.." startpage=now; /* ! now ! */
	ods rtf text = "#S={fontsize=6pt}";  

	proc report	data = Libr.&Output.&k.
				/*	style(report) = {borderbottomwidth=2pt} */
					spanrows split="|" ;
		define Name   / display style(column)={asis=on just=j width=69%} "&&TabTitle&k.";
	 %do g = 1 %to &NG.;
		define Col&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
	ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  
%mend;

																		/* Statistics Tables for All Exploratory Objectives */
%macro AllExObTables ();

	options nodate nonumber byline orientation=portrait;
	ods listing close;
	ods rtf  file = "&OutPath\&OutFile..rtf" startpage=no style=sarods contents=no notoc_data;
	
	ods rtf  path = "&OutPath"
				file = "&OutFile..rtf";

		%AnalysedDatasets  ();
		%IndexFormat       ();
		%ExObTableTemplate ();
		
	%do k = 1 %to &NExOb.;
		%ExObStatSumm   (&k.);
		%ExObReport     (&k.);
	%end;
		
	ods rtf  close;
%mend;

		%AllExObTables ();


			/** Old Version Code **/

%let HeadArr  = SubHeader;							/* Array Name for SubHeader Notes for Parameters */
%let NoteArr  = SubNotes;							/* Array Name for SubTable Notes for Visits */
%let SumArr	  = Summary;							/* Array Name for Summary Statistics */
%let SubArr	  = SubArray;							/* Array Name for SubArrays for Current Visit (&i) (for Current Parameter) */
%let SubTab   = SubTable;							/* Array Name for SubTables (for All Visits) for Current Parameter (&k) */

%let SumTemp  = TemporSum;							/* Temporary Summary File for Current Group */
													/* Formation of Analys Dataset for Differences in Interested Variables */
%macro AnalysedDataset_ ();
	data			Work.&Analys.;
		set		Libr.&Input.;
		keep		Screen_Num  Group
					&PopFlags
			%do   k = 1 %to &NExOb.;
			 %do  j = 1 %to &NSymp.;
%*			  %do i = 1 %to &NV.;
					ObPar&k.Symp&j.V1 - ObPar&k.Symp&j.V&NV.
			  %end; %end;%* %end;
					;
%*		where		&PopFl = 1;
																	/* Saving Analysing Variables */
			%do   k = 1 %to 1;
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (2, 3)  ObPar&k.Symp&j.V&i. = 1;
				 when  (1)     ObPar&k.Symp&j.V&i. = 0;
				 otherwise     ObPar&k.Symp&j.V&i. = .;
				end;
			  %end; %end; %end;
			  
			%do   k = 2 %to 2;
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (2)     ObPar&k.Symp&j.V&i. = 1;
				 when  (1, 3)  ObPar&k.Symp&j.V&i. = 0;
				 otherwise     ObPar&k.Symp&j.V&i. = .;
				end;
			  %end; %end; %end;
			  
			%do   k = 3 %to 3;
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (3)     ObPar&k.Symp&j.V&i. = 1;
				 when  (1, 2)  ObPar&k.Symp&j.V&i. = 0;
				 otherwise     ObPar&k.Symp&j.V&i. = .;
				end;
			  %end; %end; %end;
			  
			%do   k = 4 %to 4;
			 %do  j = 1 %to &NSymp.;
			  %do i = 1 %to &NV.;
				select (V&i._&Symp.&j.);
				 when  (1)     ObPar&k.Symp&j.V&i. = 1;
				 when  (2, 3)  ObPar&k.Symp&j.V&i. = 0;
				 otherwise     ObPar&k.Symp&j.V&i. = .;
				end;
			  %end; %end; %end;
	run;
%mend;
*			%AnalysedDataset_ ();

																			/* Summary Statistics for Exploratory Objectives */
%macro ExObStatSumm (InData, SumData, NumVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.;
					  output;
	run;

	%do g = 1 %to &NG;
																			/* Summary Statistics for Current Group */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
	
				output	out  = &SumTemp.
							n 		(&NumVar) = N&g.
							sum 	(&NumVar) = Sum&g. 	
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data			&SumData.;
			merge		&SumData.
						&SumTemp.;
			/* No BY: Single Line */
		run;

	%end;
%mend;
																				/* Statistics Table for Exploratory Objectives */
%macro ExObStatTable_ (SumData, SubTab, SubName);
		
		data			&SubTab.;
			length	Name $200;
						Name = &SubName.;
		run;

	%do g = 1 %to &NG;
																							/* Descriptive Statistics */
		data			&SumTemp.;
			length	Number&g. $20;
			set		&SumData.;
		
			 			Percent = Sum&g./N&g.*100;

						Number&g. = strip(put(Sum&g. , CommaX5.0)) ||
							 " (" || strip(put(Percent, CommaX5.0)) || "%)";
		run;

		data			&SubTab.;
			merge		&SubTab.
						&SumTemp.;
	%end;
%mend;
																					/* Header Lines for SubArrays */
/*
%macro ExObSubNoteLines (NoteArr);
	%do i = 1 %to &NV. ;
		data			&NoteArr.&i.;
			length	Name $200
						Number1 - Number&NG. $20;
		
						Name = &&Note&i.;  output;
		run;
	%end;
%mend;
*/
																					 /* Concatinating SubArrays for All Visits together */
%macro ConcExObStatTables (SubArr, SubTab);
	data			&SubTab.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do i = 1 %to &NV. ;
				/*	&NoteArr.&i.*/
					&SubArr.&i.
			%end;
					;
	run;
%mend;
																					/* Header Lines for SubTables */
%macro ExObSubHeadLines (HeadArr);
	%do j = 1 %to &NSymp.;
		data			&HeadArr.&j.;
			length	Name $200
						Number1 - Number&NG. $20;
		
				if (&j. > 1) then do;
						Name = "";  output;
			 	end;
						Name = &&Head&j.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubTables for All Symptoms together */
%macro ConcExObSubTables (HeadArr, SubTab, OutData);
	data			&OutData.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do j = 1 %to &NSymp.;
					&HeadArr.&j.
					&SubTab.&j.
			%end;
					;
	run;
%mend;
																		/* Statistics Tables for All Symptoms and Visits */
																				/* for Specified Exploratory Objective */
%macro ExObTable (InData, SumArr, SubArr, SubTab, HeadArr, ExObPar, OutData);
%*			%ExObSubNoteLines  (&NoteArr.);
			%ExObSubHeadLines  (&HeadArr.);
	%do j = 1 %to &NSymp.;
		%do i = 1 %to &NV.;
			%ExObStatSumm      (&InData.,    &SumArr.&i., &ExObPar.Symp&j.V&i.);
			%ExObStatTable_    (&SumArr.&i., &SubArr.&i., &&Note&i.);
		%end;
			%ConcExObStatTables(&SubArr.   , &SubTab.&j.);
	%end;
			%ConcExObSubTables (&HeadArr.  , &SubTab.   , &OutData.);
%mend;

													/* Printing Results for Specified Exploratory Objective */
%macro ExObReport (k);

	ods rtf text = "#S={just=j}&&Title&k.." startpage=now; /* ! now ! */
	ods rtf text = "#S={fontsize=6pt}";  

	proc report	data = Libr.&Output.&k.
				/*	style(report) = {borderbottomwidth=2pt} */
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "&&TabTitle&k.";
	 %do g = 1 %to &NG.;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
	ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  
%mend;

																		/* Statistics Tables for All Exploratory Objectives */
%macro AllExObTables_ ();

	options nodate nonumber byline orientation=portrait;
	ods listing close;
	ods rtf  file = "&OutPath\&OutFile..rtf" startpage=no style=sarods contents=no notoc_data;
	
	ods rtf  path = "&OutPath"
				file = "&OutFile..rtf";

	%do   k = 1 %to &NExOb.;
		%ExObTable  (Work.&Analys., Work.&SumArr.,
						 Work.&SubArr.  , Work.&SubTab., Work.&HeadArr.,
						 ObPar&k., Libr.&Output.&k.);
		%ExObReport (&k.);
	%end;
		
	ods rtf  close;
%mend;

*		%AllExObTables_ ();
