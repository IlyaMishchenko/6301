﻿														/* Evaluation_MD.sas */
										/* Formation of Medical Diagnosis (Anamnesis) Table for FAS Population */

%let PopFl = FASFL;												/* FAS Population */
%let PN    = 1;													/* FAS Population Number */

%let Input   =  Data_MD;										/* Input  data */
%let Output  =  MD;												/* Output data Name Basis */

																		/* Output File Name */
%let OutFile =  Приложение_7_Анамнез_основного_заболевания;

%let HeadArr =  SubNotes;										/* Array Name for SubTable Notes */
%let SumArr	 =  Summary;										/* Array Name for Summary Statistics */
%let SubArr	 =  SubTable;										/* Array Name for SubTables      */

%let SumTemp =  TemporSum;										/* Temporary Summary File for Current Group */

*%let MDVar  =  DurMD  SevMD;
																		/* Title Note for DurMD and SevMD */
%let Note1    = "Продолжительность симптомов ОРВИ/гриппа, часы";
%let Note2    = "Степень тяжести";

																		/* Values Names for Categorical Variables  */
%let NameCom0   = "&Tab.n / nmiss";							/* Common */

%let NCLSevMD   =  2;											/* Number of Classification Levels for SevMD */
%let NameSevMD1 = "&Tab.Лёгкая";
%let NameSevMD2 = "&Tab.Средняя";
																		/* Statistics Names for Numeric Variables (DurMD) */
%let NameNum1   = "&Tab.n / nmiss";
%let NameNum2   = "&Tab.Среднее (СО)";
%let NameNum3   = "&Tab.95%-ДИ для среднего";
%let NameNum4   = "&Tab.Медиана";
%let NameNum5   = "&Tab.Q1; Q3";
%let NameNum6   = "&Tab.Мин; Макс";

%let Title    = Таблица 10.7. Анамнез основного заболевания. Популяция всех включенных пациентов;
%let FootNote = Проценты указываются относительно количества пациентов в соответствующей группе;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */

																	/* Delite Temporary Files from Working Library */
*proc datasets	lib		= Work
					memtype	= data
					kill
					nolist;
*quit;
																			/* Header Lines for Subtables */
%macro SubHeadLines (HeadArr);
	data			&HeadArr.1
					&HeadArr.2
					;

		length	Name $200
					Number1 - Number&NG. $20;
		 
					Name = &Note1;
		 output	&HeadArr.1;
	
					Name = "";
		 output	&HeadArr.2;
					Name = &Note2;
		 output	&HeadArr.2;
	run;
%mend;
																			/* Summary Statistics for Categorical Variables */
%macro CatStatSumm (InData, SumData, CatVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.T;
						output;
				/* Empty Line */
	run;
																			/* Creating Template Dataset with All Variable Values */
																			/* for Missings Preventing										*/
	data			&SumData.P;
				do &CatVar = 1 to &&NCL&CatVar..;
						output;
				end;
	run;

	%do g = 1 %to &NG;
																			/* Total Statistics for Current Group        */
																			/* Without Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				output	out  = &SumTemp.
							n 		(&CatVar) = n&g.
							nmiss	(&CatVar) = nmiss&g.
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data				&SumData.T;
				merge		&SumData.T
							&SumTemp.;
			/* No BY: Single Line */
		run;
																			/* Partial Statistics for Current Group   */
																			/* With Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				class		&CatVar;
				ways		1;
				output	out  = &SumTemp.;
		run;
																			/* Merging Partial Statistics for All Groups Together */
		data				&SumData.P;
		/*!*/	merge		&SumData.P
							&SumTemp.
						 ( rename =
						  (_Freq_ = Freq&g.));
		/*!*/	by			&CatVar.;
		run;
	%end;
%mend;
																			/* Summary Statistics for Numerical Variables */
%macro NumStatSumm (InData, SumData, NumVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.;
					  output;
	run;

	%do g = 1 %to &NG;
																			/* Summary Statistics for Current Group */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
	
				output	out  = &SumTemp.
							n 		(&NumVar) = N&g.
							nmiss	(&NumVar) = Nmiss&g.	
							mean 	(&NumVar) = Mean&g. 	
							std	(&NumVar) = Std&g.	
							LCLM	(&NumVar) = LCLM&g.	
							UCLM	(&NumVar) = UCLM&g.	
							median(&NumVar) = Median&g.
							q1		(&NumVar) = Q1&g.		
							q3		(&NumVar) = Q3&g.		
							min	(&NumVar) = Min&g.	
							max	(&NumVar) = Max&g.	
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data			&SumData.;
			merge		&SumData.
						&SumTemp.;
			/* No BY: Single Line */
		run;
	%end;
%mend;
																			/* Statistics Table for Categorical Variables */
%macro CatStatTable (SumData, SubTab, CatVar);
																/* Total Statistics (Without Classification by Variable Values) */
	data			&SubTab.T;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.T;
	
					Name = &NameCom0;
			%do g = 1 %to &NG;
					Number&g. = strip(put(n&g.     , CommaX5.0)) ||
						" / " || strip(put(nmiss&g. , CommaX5.0))  ;
			%end;
	run;
																/* Partial Statistics (With Classification by Variable Values) */
	data			&SubTab.P;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.P;
																/* Asigning Text Names to Categorical Variable Values */
		select  (&CatVar);
			%do l = 1 %to &&NCL&CatVar..;				/* Number of Classification Levels for Specified Variable */
				when (&l.) do;
					Name =  &&Name&CatVar.&l..;		/* Name of Classification Level for Specified Variable */
				end;
			%end;
		end;
		
			%do g = 1 %to &NG;
			 if  (Freq&g. = .)
			  then do;
					Freq&g. = 0;
					Percent = 0;
			  end;
			 else	Percent = Freq&g./&&TotP&PN.&g.*100;

					Number&g. = strip(put(Freq&g., CommaX5.0)) ||
						 " (" || strip(put(Percent, CommaX5.1)) || "%)";
			%end;
	run;
	
	data			&SubTab.;
		set		&SubTab.T
					&SubTab.P;
	run;
%mend;
																				/* Statistics Table for Numerical Variables */
%macro NumStatTable (SumData, SubTab);
		
		data			&SubTab.;
			length	Name $200;
						
						Name = &NameNum1;  output;
						Name = &NameNum2;  output;
						Name = &NameNum3;  output;
						Name = &NameNum4;  output;
						Name = &NameNum5;  output;
						Name = &NameNum6;  output;
		run;

	%do g = 1 %to &NG;
																							/* Descriptive Statistics */
		data			&SumTemp.;
			length	Number&g. $20;
			set		&SumData.;
		
						Number&g. = strip(put(N&g.     , CommaX5.0)) ||
							" / " || strip(put(Nmiss&g. , CommaX5.0)) ;
							output;
						Number&g. = strip(put(Mean&g.  , CommaX5.1)) ||
							 " (" || strip(put(Std&g.   , CommaX5.1)) || ")";
							output;
						Number&g. = strip(put(LCLM&g.  , CommaX5.1)) ||
							" – " || strip(put(UCLM&g.  , CommaX5.1)) ;
							output;
						Number&g. = strip(put(Median&g., CommaX5.0)) ;
							output;
						Number&g. = strip(put(Q1&g.    , CommaX5.0)) ||
							 "; " || strip(put(Q3&g.    , CommaX5.0)) ;
							output;
						Number&g. = strip(put(Min&g.   , CommaX5.0)) ||
							 "; " || strip(put(Max&g.   , CommaX5.0)) ;
							output;
		run;

		data			&SubTab.;
			merge		&SubTab.
						&SumTemp.;
	%end;
%mend;

																				/* Summary Statistics for All Variables */
%macro AllStatSumm  ( InData,   SumArr );
		%NumStatSumm  (&InData., &SumArr.1, DurMD );				/* Formation of Summary Statistics for Sex	 */
		%CatStatSumm  (&InData., &SumArr.2, SevMD );				/* Formation of Summary Statistics for Race	 */
%mend;
																				/* Statistics Table for All Variables */
%macro AllStatTable ( SumArr,    SubArr );
		%NumStatTable (&SumArr.1, &SubArr.1        );			/* Creating Main Body for Sex	  */
		%CatStatTable (&SumArr.2, &SubArr.2, SevMD );			/* Creating Main Body for Race	*/
%mend;

																					 /* Concatinating SubTable for All Variables together */
%macro ConcStatTable (HeadArr, SubArr, OutData);
	data			&OutData.;
		keep		 Name
					 Number1 - Number&NG.;
		format	 Name
					 Number1 - Number&NG.
					 Number1 ;
		set		&HeadArr.1	&SubArr.1
					&HeadArr.2	&SubArr.2
					;
	run;
%mend;
																							/* Creating Output Table */
%macro StatTable (InData, OutData, HeadArr, SumArr, SubArr);
		%SubHeadLines	(&HeadArr.)													/* Creating Header        Lines for SubTables     */
		%AllStatSumm	(&InData. , &SumArr.);									/* Creating Summary       Table for All Variables */
		%AllStatTable	(&SumArr. , &SubArr.);									/* Creating Statistics    Table for All Variables */
		%ConcStatTable	(&HeadArr., &SubArr., &OutData.);					/* Creating Concatinating Table */
%mend;
																							/* Evaluating Output Table */																								
		%StatTable ( Libr.&Input., Libr.&Output., Work.&HeadArr., Work.&SumArr., Work.&SubArr.);

																							/* Delite Temporary Files from Working Library */
*proc datasets	lib		= Work
					memtype	= data
					kill
					nolist;
*quit;
																							/* Printing Output Table in rtf-format */
options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data;

ods rtf path = "&OutPath"
		  file = "&OutFile..rtf";

ods rtf text="#S={just=j}&Title." startpage=now;  
ods rtf text="#S={fontsize=6pt}";

%macro GroupsReport1 ();
	proc report	data = Libr.&Output.
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "";
	 %do g = 1 %to &NG;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
%mend;
		 %GroupsReport1 ();

ods rtf text="#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  

ods rtf close;
