﻿														/* Evaluation_PE.sas */
								/* Evaluation of Deviations Statistics for Physical Examinations */

%let PopFl  =  FASFL;								/* FAS Population */
%let PN     =  1;										/* FAS Population Number */

%let Input    = Data_PE;							/* Input data */
%let Analysed = Analyzed_PE;						/* Analysed data */
%let Output   = PhysExam;							/* Name for Output data */

															/* Output File Name */
%let OutFile =  Приложение_6_Физикальный_осмотр;

%let HeadArr  = SubHeader;							/* Array Name for SubHeader Notes for Parameters */
%let NoteArr  = SubNotes;							/* Array Name for SubTable Notes for Visits */
%let SumArr	  = Summary;							/* Array Name for Summary Statistics */
%let SubArr	  = SubArray;							/* Array Name for SubArrays for Current Visit (&i) (for Current Parameter) */
%let SubTab   = SubTable;							/* Array Name for SubTables (for All Visits) for Current Parameter (&k) */

%let SumTemp  = TemporSum;							/* Temporary Summary File for Current Group */

*%let NPE  = 11;										/* Number of Physical Examinations (&k) */
*%let NG   = 2;										/* Number of Groups (&g) */
*%let NV   = 4;										/* Number of Visits (&i) */

%let PESt  = 1;										/* Step between Visits for Physical Examinations */
																	/* Analysing Variables */
*%let PEPar1  =  APPEAR;
*%let PEPar2  =  SKIN  ;
*%let PEPar3  =  LOR   ;
*%let PEPar4  =  EYE   ;
*%let PEPar5  =  RESP  ;
*%let PEPar6  =  CV    ;
*%let PEPar7  =  GASTR ;
*%let PEPar8  =  MOTOR ;
*%let PEPar9  =  ENDOCR;
*%let PEPar10 =  NEURO ;
*%let PEPar11 =  REN   ;
*										/* Physical Examination Types */
*%let PEType1 = NRIND;			/* Normality Identification */
*%let PEType2 = CLSIG;			/* Clinical Significance (of Deviation) */
*%let PEType3 = ORRES;			/* Operation Result (Description) */
																	/* Head  Notes for Parameters */
%let Head1 = "Общее состояние";
%let Head2 = "Состояние кожных покровов и видимых слизистых оболочек";
%let Head3 = "ЛОР органы";
%let Head4 = "Глаза";
%let Head5 = "Дыхательная система";
%let Head6 = "Сердечно-сосудистая система";
%let Head7 = "Пищеварительная система";
%let Head8 = "Опорно-двигательная система";
%let Head9 = "Эндокринная система";
%let Head10= "Нервная система";
%let Head11= "Мочевыделительная система";
																	/* Title Notes for Visits: */
%let Note1 = "&Tab.День 1";
%let Note2 = "&Tab.День 3";
%let Note3 = "&Tab.День 6";
%let Note4 = "&Tab.День 14";

%let NCL  =  3;												/* Number of Classification Levels for Analysing Variables */
																	/* Statistics Names for Variables */
%let Name0 = "&Tab.&Tab.n / nmiss";
%let Name1 = "&Tab.&Tab.Норма";
%let Name2 = "&Tab.&Tab.Незначимое отклонение";
%let Name3 = "&Tab.&Tab.Значимое отклонение";
																			/* Title for Physical Examinations */
%let Title = Таблица 10.6. Исследование по системам органов. Статистика отклонений от нормы.
Популяция всех включенных пациентов;
%let FootNote = Проценты указываются относительно количества пациентов в соответствующей группе;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */

													/* Formation of Analysed Dataset for Differences in Interested Variables */
%macro AnalysedDataset ();
	data			Libr.&Analysed.;
		set		Libr.&Input.;
		keep		Screen_Num  Group
					&PopFlags
			%do k = 1 %to &NPE;
					&&PEPar&k..1 - &&PEPar&k..&NV.
			%end;
					;
%*		where		&PopFl = 1;
																	/* Saving Analysing Variables */
			%do k = 1 %to &NPE;
				%do i = 1 %to &NV %by &PESt; /* ! %by &PESt ! */
					if (V&i._PE&PEType1._&&PEPar&k.. = "NORMAL") then
						&&PEPar&k..&i. = 1;
					if (V&i._PE&PEType2._&&PEPar&k.. = "N") then
						&&PEPar&k..&i. = 2;
					if (V&i._PE&PEType2._&&PEPar&k.. = "Y") then
						&&PEPar&k..&i. = 3;
				%end;
			%end;
	run;
%mend;
			%AnalysedDataset ();

																			/* Summary Statistics for Categorical Variables */
%macro CatStatSumm (InData, SumData, CatVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.T;
						output;
				/* Empty Line */
	run;
																			/* Creating Template Dataset with All Variable Values */
																			/* for Missings Preventing										*/
	data			&SumData.P;
				do &CatVar = 1 to &NCL;
						output;
				end;
	run;

	%do g = 1 %to &NG;
																			/* Total Statistics for Current Group        */
																			/* Without Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				output	out  = &SumTemp.
							n 		(&CatVar) = n&g.
							nmiss	(&CatVar) = nmiss&g.
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data				&SumData.T;
				merge		&SumData.T
							&SumTemp.;
			/* No BY: Single Line */
		run;
																			/* Partial Statistics for Current Group   */
																			/* With Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				class		&CatVar;
				ways		1;
				output	out  = &SumTemp.;
		run;
																			/* Merging Partial Statistics for All Groups Together */
		data				&SumData.P;
		/*!*/	merge		&SumData.P
							&SumTemp.
						 ( rename =
						  (_Freq_ = Freq&g.));
		/*!*/	by			&CatVar.;
		run;
	%end;
%mend;
																			/* Statistics Table for Categorical Variables */
%macro CatStatTable (SumData, SubTab, CatVar);
																/* Total Statistics (Without Classification by Variable Values) */
	data			&SubTab.T;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.T;
	
					Name = &Name0;
			%do g = 1 %to &NG;
					Number&g. = strip(put(n&g.     , CommaX5.0)) ||
						" / " || strip(put(nmiss&g. , CommaX5.0))  ;
			%end;
	run;
																/* Partial Statistics (With Classification by Variable Values) */
	data			&SubTab.P;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.P;
																/* Asigning Text Names to Categorical Variable Values */
		select  (&CatVar);
			%do l = 1 %to &NCL;				/* Number of Classification Levels for Specified Variable */
				when (&l.) do;
					Name =  &&Name&l..;		/* Name of Classification Level for Specified Variable */
				end;
			%end;
		end;
		
			%do g = 1 %to &NG;
			 if  (Freq&g. = .)
			  then do;
					Freq&g. = 0;
					Percent = 0;
			  end;
			 else	Percent = Freq&g./&&TotP&PN.&g.*100;

					Number&g. = strip(put(Freq&g., CommaX5.0)) ||
						 " (" || strip(put(Percent, CommaX5.1)) || "%)";
			%end;
	run;
	
	data			&SubTab.;
		set		&SubTab.T
					&SubTab.P;
	run;
%mend;
																					/* Header Lines for SubArrays */
%macro SubNoteLines (NoteArr);
	%do i = 1 %to &NV %by &PESt;
		data			&NoteArr.&i.;
			length	Name $200
						Number1 - Number&NG. $20;
		
						Name = &&Note&i.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubArrays for All Visits together */
%macro ConcStatTables (NoteArr, SubArr, SubTab);
	data			&SubTab.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do i = 1 %to &NV %by &PESt;
					&NoteArr.&i.	&SubArr.&i.
			%end;
					;
	run;
%mend;
																					/* Header Lines for SubTables */
%macro SubHeadLines (HeadArr);
	%do k = 1 %to &NPE;
		data			&HeadArr.&k.;
			length	Name $200
						Number1 - Number&NG. $20;
		
				if (&k. > 1) then do;
						Name = "";  output;
			 	end;
						Name = &&Head&k.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubTables for All Parameters together */
%macro ConcSubTables (HeadArr, SubTab, OutData);
	data			&OutData.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do k = 1 %to &NPE;
					&HeadArr.&k.	&SubTab.&k.
			%end;
					;
	run;
%mend;
																				/* Summary Statistics for All Variables and Visits */
%macro StatTable     (InData, SumArr, SubArr, SubTab, NoteArr, HeadArr, OutData);
			%SubNoteLines  (&NoteArr.);
			%SubHeadLines  (&HeadArr.);
	%do k = 1 %to &NPE;
		%do i = 1 %to &NV %by &PESt;
			%CatStatSumm   (&InData.,    &SumArr.&i., &&PEPar&k..&i.);
			%CatStatTable  (&SumArr.&i., &SubArr.&i., &&PEPar&k..&i.);  /* ! CatVar ! */
		%end;
			%ConcStatTables(&NoteArr., &SubArr., &SubTab.&k.);
	%end;
			%ConcSubTables (&HeadArr., &SubTab., &OutData.);
%mend;
																				/* Summary Statistics Tables for All Variables */
		%StatTable (Libr.&Analysed., Work.&SumArr., Work.&SubArr., Work.&SubTab.,
											  Work.&NoteArr., Work.&HeadArr., Libr.&Output.);

													/* Printing Results for Physical Examinations Objectives */
%macro GroupsReport1;
	proc report	data = Libr.&Output.
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "";
	 %do g = 1 %to &NG.;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
%mend;

options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf  file = "&OutPath\&OutFile..rtf" startpage=no style=sarods contents=no notoc_data;

ods rtf  path = "&OutPath"
			file = "&OutFile..rtf";

ods rtf text = "#S={just=j}&Title" startpage=now; /* ! now ! */
ods rtf text = "#S={fontsize=6pt}";  

			%GroupsReport1; /* PE */

ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  
			
ods rtf  close;
