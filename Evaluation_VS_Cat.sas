﻿													/* Evaluation_VS_Cat.sas */
								/* Evaluation of Deviations Statistics for Vital Signs */

%let PopFl  =  FASFL;								/* FAS Population */
%let PN     =  1;										/* FAS Population Number */

%let Input    = Data_VS;							/* Input data */
%let Analysed = Analyzed_VS_Cat;					/* Analysed data */
%let Output   = VitSigns_Cat;						/* Name for Output data */

															/* Output File Name */
%let OutFile =  Приложение_5_ЖВП_Статистика_отклонений;

%let HeadArr  = SubHeader;							/* Array Name for SubHeader Notes for Parameters */
%let NoteArr  = SubNotes;							/* Array Name for SubTable Notes for Visits */
%let SumArr	  = Summary;							/* Array Name for Summary Statistics */
%let SubArr	  = SubArray;							/* Array Name for SubArrays for Current Visit (&i) (for Current Parameter) */
%let SubTab   = SubTable;							/* Array Name for SubTables (for All Visits) for Current Parameter (&k) */

%let SumTemp  = TemporSum;							/* Temporary Summary File for Current Group */

*%let NVS  = 4;										/* Number of Vital Signs (&k) */
*%let NG   = 2;										/* Number of Groups (&g) */
*%let NV   = 4;										/* Number of Visits (&i) */

%let VSSt  = 1;										/* Step between Visits for Vital Signs */

																/* Analysing Variables */
*%let VSPar1 = BP;									/* Blood Pressure */
*%let VSPar2 = HR;									/* Heart Reductions */
*%let VSPar3 = RR;									/* Respiratory Reductions */
*%let VSPar4 = TEMP;									/* Temperature */

																/* Vital Signs Types */
*%let VSType1 = ORRES;								/* Operation Result */
*%let VSType2 = NRIND;								/* Normality Identification */
*%let VSType3 = CLSIG;								/* Clinical Significance (of Deviation) */
																	/* Head  Notes for Parameters */
%let Head1 = "Артериальное давление, мм.рт.ст.";
%let Head2 = "Частота сердечных сокращений, уд/мин";
%let Head3 = "Частота дыхательных движений, в мин";
%let Head4 = "Температура тела, °С";
																	/* Title Notes for Visits: */
%let Note1 = "&Tab.День 1";
%let Note2 = "&Tab.День 3";
%let Note3 = "&Tab.День 6";
%let Note4 = "&Tab.День 14";

%let NCL  =  3;												/* Number of Classification Levels for Analysing Variables */
																	/* Statistics Names for Variables */
%let Name0 = "&Tab.&Tab.n / nmiss";
%let Name1 = "&Tab.&Tab.Норма";
%let Name2 = "&Tab.&Tab.Незначимое отклонение";
%let Name3 = "&Tab.&Tab.Значимое отклонение";
																			/* Title for Vital Signs */
%let Title = Таблица 10.5. Жизненно важные показатели. Статистика отклонений от нормы.
Популяция всех включенных пациентов;
%let FootNote = Проценты указываются относительно количества пациентов в соответствующей группе;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */

													/* Formation of Analysed Dataset for Differences in Interested Variables */
%macro AnalysedDataset ();
	data			Libr.&Analysed.;
		set		Libr.&Input.;
		keep		Screen_Num  Group
					&PopFlags
			%do k = 1 %to &NVS;
					&&VSPar&k..1 - &&VSPar&k..&NV.
			%end;
					;
%*		where		&PopFl = 1;
																	/* Saving Analysing Variables */
			%do k = 1 %to &NVS;
				%do i = 1 %to &NV %by &VSSt; /* ! %by &VSSt ! */
					if (V&i._VS&VSType2._&&VSPar&k.. = "NORMAL") then
						&&VSPar&k..&i. = 1;
					if (V&i._VS&VSType3._&&VSPar&k.. = "N") then
						&&VSPar&k..&i. = 2;
					if (V&i._VS&VSType3._&&VSPar&k.. = "Y") then
						&&VSPar&k..&i. = 3;
				%end;		 /* ! VSType2 ! */
			%end;			 /* ! VSType3 ! */
	run;
%mend;
			%AnalysedDataset ();

																			/* Summary Statistics for Categorical Variables */
%macro CatStatSumm (InData, SumData, CatVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.T;
						output;
				/* Empty Line */
	run;
																			/* Creating Template Dataset with All Variable Values */
																			/* for Missings Preventing										*/
	data			&SumData.P;
				do &CatVar = 1 to &NCL;
						output;
				end;
	run;

	%do g = 1 %to &NG;
																			/* Total Statistics for Current Group        */
																			/* Without Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				output	out  = &SumTemp.
							n 		(&CatVar) = n&g.
							nmiss	(&CatVar) = nmiss&g.
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data				&SumData.T;
				merge		&SumData.T
							&SumTemp.;
			/* No BY: Single Line */
		run;
																			/* Partial Statistics for Current Group   */
																			/* With Classification by Variable Values */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
				class		&CatVar;
				ways		1;
				output	out  = &SumTemp.;
		run;
																			/* Merging Partial Statistics for All Groups Together */
		data				&SumData.P;
		/*!*/	merge		&SumData.P
							&SumTemp.
						 ( rename =
						  (_Freq_ = Freq&g.));
		/*!*/	by			&CatVar.;
		run;
	%end;
%mend;
																			/* Statistics Table for Categorical Variables */
%macro CatStatTable (SumData, SubTab, CatVar);
																/* Total Statistics (Without Classification by Variable Values) */
	data			&SubTab.T;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.T;
	
					Name = &Name0;
			%do g = 1 %to &NG;
					Number&g. = strip(put(n&g.     , CommaX5.0)) ||
						" / " || strip(put(nmiss&g. , CommaX5.0))  ;
			%end;
	run;
																/* Partial Statistics (With Classification by Variable Values) */
	data			&SubTab.P;
		length	Name $200
					Number1 - Number&NG. $20;
		set		&SumData.P;
																/* Asigning Text Names to Categorical Variable Values */
		select  (&CatVar);
			%do l = 1 %to &NCL;				/* Number of Classification Levels for Specified Variable */
				when (&l.) do;
					Name =  &&Name&l..;		/* Name of Classification Level for Specified Variable */
				end;
			%end;
		end;
		
			%do g = 1 %to &NG;
			 if  (Freq&g. = .)
			  then do;
					Freq&g. = 0;
					Percent = 0;
			  end;
			 else	Percent = Freq&g./&&TotP&PN.&g.*100;

					Number&g. = strip(put(Freq&g., CommaX5.0)) ||
						 " (" || strip(put(Percent, CommaX5.1)) || "%)";
			%end;
	run;
	
	data			&SubTab.;
		set		&SubTab.T
					&SubTab.P;
	run;
%mend;
																					/* Header Lines for SubArrays */
%macro SubNoteLines (NoteArr);
	%do i = 1 %to &NV %by &VSSt;
		data			&NoteArr.&i.;
			length	Name $200
						Number1 - Number&NG. $20;
		
						Name = &&Note&i.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubArrays for All Visits together */
%macro ConcStatTables (NoteArr, SubArr, SubTab);
	data			&SubTab.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do i = 1 %to &NV %by &VSSt;
					&NoteArr.&i.	&SubArr.&i.
			%end;
					;
	run;
%mend;
																					/* Header Lines for SubTables */
%macro SubHeadLines (HeadArr);
	%do k = 1 %to &NVS;
		data			&HeadArr.&k.;
			length	Name $200
						Number1 - Number&NG. $20;
		
				if (&k. > 1) then do;
						Name = "";  output;
			 	end;
						Name = &&Head&k.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubTables for All Parameters together */
%macro ConcSubTables (HeadArr, SubTab, OutData);
	data			&OutData.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do k = 1 %to &NVS;
					&HeadArr.&k.	&SubTab.&k.
			%end;
					;
	run;
%mend;
																				/* Summary Statistics for All Variables and Visits */
%macro StatTable     (InData, SumArr, SubArr, SubTab, NoteArr, HeadArr, OutData);
			%SubNoteLines  (&NoteArr.);
			%SubHeadLines  (&HeadArr.);
	%do k = 1 %to &NVS;
		%do i = 1 %to &NV %by &VSSt;
			%CatStatSumm   (&InData.,    &SumArr.&i., &&VSPar&k..&i.);
			%CatStatTable  (&SumArr.&i., &SubArr.&i., &&VSPar&k..&i.);  /* ! CatVar ! */
		%end;
			%ConcStatTables(&NoteArr., &SubArr., &SubTab.&k.);
	%end;
			%ConcSubTables (&HeadArr., &SubTab., &OutData.);
%mend;
																				/* Summary Statistics Tables for All Variables */
		%StatTable (Libr.&Analysed., Work.&SumArr., Work.&SubArr., Work.&SubTab.,
											  Work.&NoteArr., Work.&HeadArr., Libr.&Output.);

													/* Printing Results for Vital Signs Objectives */
%macro GroupsReport1;
	proc report	data = Libr.&Output.
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "";
	 %do g = 1 %to &NG.;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
%mend;

options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf  file = "&OutPath\&OutFile..rtf" startpage=no style=sarods contents=no notoc_data;

ods rtf  path = "&OutPath"
			file = "&OutFile..rtf";

ods rtf text = "#S={just=j}&Title" startpage=now; /* ! now ! */
ods rtf text = "#S={fontsize=6pt}";  

			%GroupsReport1; /* VS */

ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  
			
ods rtf  close;
