﻿													/* Evaluation_VS_Num.sas */
								/* Evaluation of Descriptive Statistics for Vital Signs */

%global	Dig;											/* Number of Significant Digits for "Rough" Statistics */

%let PopFl  =  FASFL;								/* FAS Population */
%let PN     =  1;										/* FAS Population Number */

%let Input    = Data_VS;							/* Input data */
%let Analysed = Analyzed_VS_Num;					/* Analysed data */
%let Output   = VitSigns_Num;						/* Name for Output data */

															/* Output File Name */
%let OutFile =  Приложение_4_ЖВП_Описательная_статистика;

%let HeadArr  = SubHeader;							/* Array Name for SubHeader Notes for Parameters */
%let NoteArr  = SubNotes;							/* Array Name for SubTable Notes for Visits */
%let SumArr	  = Summary;							/* Array Name for Summary Statistics */
%let SubArr	  = SubArray;							/* Array Name for SubArrays for Current Visit (&i) (for Current Parameter) */
%let SubTab   = SubTable;							/* Array Name for SubTables (for All Visits) for Current Parameter (&k) */

%let SumTemp  = TemporSum;							/* Temporary Summary File for Current Group */

*%let NVS  = 4;										/* Number of Vital Signs (&k) */
*%let NG   = 2;										/* Number of Groups (&g) */
*%let NV   = 4;										/* Number of Visits (&i) */

%let VSSt  = 1;										/* Step between Visits for Vital Signs */

													/* Additional Variable for Sistolic Blood Pressure */

%let VSPar0 = BPS;									/* Blood Pressure Systolic  */
*%let VSPar1 = BP;									/* Blood Pressure Diastolic */

																/* Analysing Variables */

**%let VSPar0 = BPS;									/* Blood Pressure Systolic  */
**%let VSPar1 = BPD;									/* Blood Pressure Diastolic */

*%let VSPar1 = BP;									/* Blood Pressure */
*%let VSPar2 = HR;									/* Heart Reductions */
*%let VSPar3 = RR;									/* Respiratory Reductions */
*%let VSPar4 = TEMP;									/* Temperature */

																/* Vital Signs Types */
*%let VSType1 = ORRES;								/* Operation Result */
*%let VSType2 = NRIND;								/* Normality Identification */
*%let VSType3 = CLSIG;								/* Clinical Significance (of Deviation) */

															/* Head  Notes for Parameters */
%let Head0 = "Систолическое артериальное давление, мм.рт.ст.";
%let Head1 = "Диастолическое артериальное давление, мм.рт.ст.";
%let Head2 = "Частота сердечных сокращений, уд/мин";
%let Head3 = "Частота дыхательных движений, в мин";
%let Head4 = "Температура тела, °С";
																	/* Title Notes for Visits: */
%let Note1 = "&Tab.День 1";
%let Note2 = "&Tab.День 3";
%let Note3 = "&Tab.День 6";
%let Note4 = "&Tab.День 14";

*%let NCL  =  3;												/* Number of Classification Levels for Analysing Variables */
																	/* Statistics Names for Variables */
%let Name0 = "&Tab.&Tab.n / nmiss";
%let Name1 = "&Tab.&Tab.Норма";
%let Name2 = "&Tab.&Tab.Незначимое отклонение";
%let Name3 = "&Tab.&Tab.Значимое отклонение";
																		/* Statistics Names for Numeric Variables (Age) */
%let NameNum1 = "&Tab.&Tab.n / nmiss";
%let NameNum2 = "&Tab.&Tab.Среднее (СО)";
%let NameNum3 = "&Tab.&Tab.95%-ДИ для среднего";
%let NameNum4 = "&Tab.&Tab.Медиана";
%let NameNum5 = "&Tab.&Tab.Q1; Q3";
%let NameNum6 = "&Tab.&Tab.Мин; Макс";
																			/* Title for Vital Signs */
%let Title = Таблица 10.4. Жизненно важные показатели. Описательная статистика.
Популяция всех включенных пациентов;
*%let FootNote = Проценты указываются относительно количества пациентов в соответствующей группе;

&TitleSAR;	&FootnoteSAR;					/* Title and Footnote for Document, modified by &OutFile.-MacroVariable */

													/* Formation of Analysed Dataset for Differences in Interested Variables */
%macro AnalysedDataset ();
	data			Libr.&Analysed.;
		set		Libr.&Input.;
		keep		Screen_Num  Group
					&PopFlags
			%do k = 0 %to &NVS.;
					&&VSPar&k..1 - &&VSPar&k..&NV.
			%end;
					;
%*		where		&PopFl = 1;
																	/* Saving Analysing Variables */
			%do k = 1 %to 1; /* ! %by &VSSt. ! */
				%do i = 1 %to &NV. %by &VSSt.;
						 &VSPar0.&i.   = V&i._VS&VSType1._&&VSPar&k..S ;
				%end;							  /* ! VSType1 ! */
				%do i = 1 %to &NV. %by &VSSt.;
						 &VSPar1.&i.   = V&i._VS&VSType1._&&VSPar&k..D ;
				%end;
			%end;
			%do k = 2 %to &NVS.;
				%do i = 1 %to &NV. %by &VSSt.;
						&&VSPar&k..&i. = V&i._VS&VSType1._&&VSPar&k..  ;
				%end;
			%end;
	run;
%mend;
			%AnalysedDataset ();

																			/* Summary Statistics for Numerical Variables */
%macro NumStatSumm (InData, SumData, NumVar);
																			/* Creating Initial Dataset with Empty Line */
	data			&SumData.;
					  output;
	run;

	%do g = 1 %to &NG;
																			/* Summary Statistics for Current Group */
		proc summary	data = &InData.;
				where		&PopFl and
						  (&g. = 0 or Group = &g.);
	
				output	out  = &SumTemp.
							n 		(&NumVar) = N&g.
							nmiss	(&NumVar) = Nmiss&g.	
							mean 	(&NumVar) = Mean&g. 	
							std	(&NumVar) = Std&g.	
							LCLM	(&NumVar) = LCLM&g.	
							UCLM	(&NumVar) = UCLM&g.	
							median(&NumVar) = Median&g.
							q1		(&NumVar) = Q1&g.		
							q3		(&NumVar) = Q3&g.		
							min	(&NumVar) = Min&g.	
							max	(&NumVar) = Max&g.	
							;
		run;
																			/* Merging Total Statistics for All Groups Together */
		data			&SumData.;
			merge		&SumData.
						&SumTemp.;
			/* No BY: Single Line */
		run;
	%end;
%mend;
																				/* Statistics Table for Numerical Variables */
%macro NumStatTable (SumData, SubTab);
		
		data			&SubTab.;
			length	Name $200;
						
						Name = &NameNum1;  output;
						Name = &NameNum2;  output;
						Name = &NameNum3;  output;
						Name = &NameNum4;  output;
						Name = &NameNum5;  output;
						Name = &NameNum6;  output;
		run;

	%do g = 0 %to &NG;
																							/* Descriptive Statistics */
		data			&SumTemp.;
			length	Number&g. $20;
			set		&SumData.;
		
						Number&g. = strip(put(N&g.     , CommaX5.0)) ||
							" / " || strip(put(Nmiss&g. , CommaX5.0)) ;
							output;
						Number&g. = strip(put(Mean&g.  , CommaX5.1)) ||
							 " (" || strip(put(Std&g.   , CommaX5.1)) || ")";
							output;
						Number&g. = strip(put(LCLM&g.  , CommaX5.1)) ||
							" – " || strip(put(UCLM&g.  , CommaX5.1)) ;
							output;
						Number&g. = strip(put(Median&g., CommaX5.&Dig.)) ;
							output;
						Number&g. = strip(put(Q1&g.    , CommaX5.&Dig.)) ||
							 "; " || strip(put(Q3&g.    , CommaX5.&Dig.)) ;
							output;
						Number&g. = strip(put(Min&g.   , CommaX5.&Dig.)) ||
							 "; " || strip(put(Max&g.   , CommaX5.&Dig.)) ;
							output;
		run;

		data			&SubTab.;
			merge		&SubTab.
						&SumTemp.;
	%end;
%mend;
																					/* Header Lines for SubArrays */
%macro SubNoteLines (NoteArr);
	%do i = 1 %to &NV. %by &VSSt. ;
		data			&NoteArr.&i.;
			length	Name $200
						Number1 - Number&NG. $20;
		
						Name = &&Note&i.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubArrays for All Visits together */
%macro ConcStatTables (NoteArr, SubArr, SubTab);
	data			&SubTab.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do i = 1 %to &NV. %by &VSSt. ;
					&NoteArr.&i.	&SubArr.&i.
			%end;
					;
	run;
%mend;
																					/* Header Lines for SubTables */
%macro SubHeadLines (HeadArr);
	%do k = 0 %to &NVS.;
		data			&HeadArr.&k.;
			length	Name $200
						Number1 - Number&NG. $20;
		
				if (&k. > 0) then do;
						Name = "";  output;
			 	end;
						Name = &&Head&k.;  output;
		run;
	%end;
%mend;
																					 /* Concatinating SubTables for All Parameters together */
%macro ConcSubTables (HeadArr, SubTab, OutData);
	data			&OutData.;
		keep		Name
					Number1 - Number&NG.;
		set
			%do k = 0 %to &NVS.;
					&HeadArr.&k.	&SubTab.&k.
			%end;
					;
	run;
%mend;
																				/* Summary Statistics for All Variables and Visits */
%macro StatTable     (InData, SumArr, SubArr, SubTab, NoteArr, HeadArr, OutData);
			%SubNoteLines  (&NoteArr.);
			%SubHeadLines  (&HeadArr.);
	%do k = 0 %to &NVS.;
															/* Assigning Number of Significant Digits for "Rough" Statistics */
		%if (&k. < &NVS.) %then %do;  %let Dig = 0;  %end;
								%else %do;  %let Dig = 1;  %end;

		%do i = 1 %to &NV. %by &VSSt. ;
			%NumStatSumm   (&InData.,    &SumArr.&i., &&VSPar&k..&i.);
			%NumStatTable  (&SumArr.&i., &SubArr.&i.);
		%end;
			%ConcStatTables(&NoteArr., &SubArr., &SubTab.&k.);
	%end;
			%ConcSubTables (&HeadArr., &SubTab., &OutData.);
%mend;
																				/* Summary Statistics Tables for All Variables */
		%StatTable (Libr.&Analysed., Work.&SumArr., Work.&SubArr., Work.&SubTab.,
											  Work.&NoteArr., Work.&HeadArr., Libr.&Output.);

													/* Printing Results for Vital Signs Objectives */
%macro GroupsReport1;
	proc report	data = Libr.&Output.
					style(report) = {borderbottomwidth=2pt}
					spanrows split="|" ;
		define Name      / display style(column)={asis=on just=j width=69%} "";
	 %do g = 1 %to &NG.;
		define Number&g. / display style(column)={asis=on just=c width=15%} "&&GName&g.|&&NP&PN.&g.";
	 %end;
	run;
%mend;

options nodate nonumber byline orientation=portrait;
ods listing close;
ods rtf  file = "&OutPath\&OutFile..rtf" startpage=no style=sarods contents=no notoc_data;

ods rtf  path = "&OutPath"
			file = "&OutFile..rtf";

ods rtf text = "#S={just=j}&Title" startpage=now; /* ! now ! */
ods rtf text = "#S={fontsize=6pt}";  

			%GroupsReport1; /* VS */

*ods rtf text = "#S={fontsize=10pt outputwidth=100% borderbottomwidth=2pt just=j} &FootNote." ;  
			
ods rtf  close;
