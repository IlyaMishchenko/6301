﻿														/* Generation_EmptyFiles.sas"; */
												/* Shortcut Generation of Files Without Data */

%let Title   =  Таблица 10.2. Пациенты, досрочно выбывшие из исследования;		/* Output Title */
%let Output  =  Withdrawals;																	/* Output Dataset */
%let OutFile =  Приложение_2_Пациенты_выбывшие_из_исследования;					/* Output File Name */
%let EmpNote =  Пациенты, досрочно выбывшие из исследования, отсутствуют;		/* Emptiness Notification */
	
	%PrintEmpNote ();
/*
%let Title   =  Таблица 10.X. Пациенты, завершившие исследования с отклониниями от протокола;
%let Output  =  Deviations;
%let OutFile =  Приложение_X_Пациенты_завершившие_исследование_с_отклонениями_от_протокола;
%let EmpNote =  Пациенты, завершившие исследования с отклониниями от протокола, отсутствуют;
	
	%PrintEmpNote ();
*/
%let Title   =  Таблица 10.8. Заболевания, осложняющие течение ОРВИ. Популяция всех включенных пациентов;
%let Output  =  MH;
%let OutFile =  Приложение_8_Сопутствующие_заболевания;
%let EmpNote =  Заболевания, осложняющие течение ОРВИ, отсутствуют;
	
	%PrintEmpNote ();

%let Title   =  Таблица 10.11. Осложнения гриппа или ОРВИ. Популяция всех включенных пациентов;
%let Output  =  Seq; /* Sequelae */
%let OutFile =  Приложение_11_Осложнения;
%let EmpNote =  Осложнения гриппа и ОРВИ отсутствуют;
	
	%PrintEmpNote ();


%let Title1 =  Таблица 11.5. 5-я поисковая конечная точка.
Доля пациентов с осложнениями ОРВИ и гриппа. Популяция оценки клинического эффекта;
%let Title2 =  Таблица 11.6. 6-я поисковая конечная точка.
Доля пациентов с осложнениями ОРВИ и гриппа, потребовавших назначения антибактериальных препаратов.
Популяция оценки клинического эффекта;

%let Output  =  Obj_Seq;
%let OutFile =  Приложение_13_ИКТ_Осложнения;
%let EmpNote =  Осложнения гриппа и ОРВИ отсутствуют;
	
		%PrintOptions1 ();

ods rtf text = "#S={just=j}&Title1"  startpage=now;  
ods rtf text = "#S={fontsize=6pt}";
ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  

ods rtf text = " ";  
ods rtf text = " ";
ods rtf text = "#S={just=j}&Title2"  startpage=off;  
ods rtf text = "#S={fontsize=6pt}";
ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  

ods rtf close;


%let Title   =  Таблица 12.1. Конечная точка безопасности.
Частота нежелательных реакций и серьезных нежелательных явлений. Популяция всех включенных пациентов;
%let Output  =  Obj_Saf;
%let OutFile =  Приложение_15_КТБ_Частота_НР_и_СНЯ;
%let EmpNote =  Нежелательные явления отсутствуют;
	
	%PrintEmpNote ();

%let Title   =  Таблица 12.2. Обзор нежелательных явлений. Популяция всех включенных пациентов;
%let Output  =  AE_Common;
%let OutFile =  Приложение_16_Обзор_НЯ;
%let EmpNote =  Нежелательные явления отсутствуют;
	
	%PrintEmpNote ();


%let PopAE = 1;		/* Population Number for AE Evaluation: FAS */

%let Title;			 /* ! Will be defined for Each Group (&g.) Individually ! */
%let Output  =  AE_IPRelSev;
%let OutFile =  Приложение_17_Распределение_НЯ_по_степени_тяжести_и_связи_с_ИП;
%let EmpNote =  Нежелательные явления отсутствуют;

%macro PrintEmpNote_AE_IPRelSev ();
	
		%PrintOptionsL1 ();
	
	%do g = 1 %to &NG.;

		%let Title  =  Таблица 12.3.&g.. Распределение нежелательных явлений по степени тяжести и связи с ИП.
Группа «&&GName&g..» (&&NP&PopAE.&g..). Популяция всех включенных пациентов;

	 %if (&g. ^= 1) %then %do;
		ods rtf text = " ";  
		ods rtf text = " ";
	 %end;
		ods rtf text = "#S={just=j}&Title"  startpage=now;  
		ods rtf text = "#S={fontsize=6pt}";
		ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
	%end;
	
		ods rtf close;

%mend;

		%PrintEmpNote_AE_IPRelSev ();


%let Title   =  Таблица 12.4. Клинический анализ крови. Статистика отклонений от нормы.
Популяция всех включенных пациентов;
%let Output  =  Lab_ClBlTest;
%let OutFile =  Приложение_18_Клинический_анализ_крови;
%let EmpNote =  Клинический анализ крови не собирался;
	
	%PrintEmpNote ();

%let Title   =  Таблица 12.5. Биохимический анализ крови. Статистика отклонений от нормы.
Популяция всех включенных пациентов;
%let Output  =  Lab_BchBlTest;
%let OutFile =  Приложение_19_Биохимический_анализ_крови;
%let EmpNote =  Биохимический анализ крови не собирался;
	
	%PrintEmpNote ();

%let Title   =  Таблица 12.6. Клинический анализ мочи. Статистика отклонений от нормы.
Популяция всех включенных пациентов;
%let Output  =  Lab_ClUrTest;
%let OutFile =  Приложение_20_Клинический_анализ_мочи;
%let EmpNote =  Клинический анализ мочмочии не собирался;
	
	%PrintEmpNote ();
