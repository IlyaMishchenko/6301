﻿														/* Listings.sas */
													/* Listings Formation */

%let Temp     =  Temporary;						/* Temporary File */

proc format;
 value $ BlankFmt						/* Format for Text Blanks */
  " " = "н/п"
  "-" = "                              "
  ;										/* Safety Length Code */
 value $ BoolFmt						/* Format for Boolens */
  "True"  =  "Да"
  "False" =  "Нет" 
  " "     =  "н/п"
  ;
 value $ UnDateFmt					/* Format for Unknown Dates */
/*"U" =  "н/и"*/
  "U" =  "–"
  " " =  "н/п"
  ;
 value   YNFmtO						/* Initial First Format for Yes/No */
  1   =  "Да"
  2   =  "Нет"
  .   =  "н/п"
  ;
 value $ YNFmt							/* First Format for Yes/No */
  "Y" =  "Да"
  "N" =  "Нет"
  " " =  "н/п"
  ;
 value   NYFmt							/* Second Format for Yes/No */
  0   =  "Нет"
  1   =  "Да"
  .   =  "н/п"
  ;
 value   GrFmt							/* Format for IP Groups */
  1   =  "&GName1"
  2   =  "&GName2"
  ;
 value   SexFmt						/* Format for Sex */
  1   =  "Мужской"
  2   =  "Женский"
  ;
 value   RaceFmt						/* Format for Race */
  1   =  "Европейская"
  2   =  "Другая"
  ;
 value   VisFmt						/* Format for Visits */
  1   =  "День 1"   
  2   =  "День 3"   
  3   =  "День 6" 
  4   =  "День 14" 
  ;
 value   VSFmt							/* Format for Vital Signs */
/*1   =  "АД, мм.рт.ст."*/
  2   =  "ЧСС, уд/мин"
  3   =  "ЧДД, в мин"
  4   =  "Температура, °С"
  ;
 value   MDSevFmt						/* Format for Medical Diagnosis Sevire Degree */
  1   =  "Легкая"
  2   =  "Средняя"
  ;
 value   PEFmt							/* Format for Physical Examination */
  1   =  "Общее состояние"
  2   =  "Состояние кожных покровов"/* и видимых слизистых оболочек*/
  3   =  "ЛОР органы"
  4   =  "Глаза"
  5   =  "Дыхательная система"
  6   =  "Сердечно-сосудистая система"
  7   =  "Пищеварительная система"
  8   =  "Опорно-двигательная система"
  9   =  "Эндокринная система"
  10  =  "Нервная система"
  11  =  "Мочевыделительная система"
  ;
value   LabFmt						/* Format for Laboratory */ /* Non-Using */
  1   =  "Общий белок"       
  2   =  "Креатинин"         
  3   =  "Мочевина"          
  4   =  "Глюкоза"           
  5   =  "Холестерин общий"  
  6   =  "Билирубин общий"   
  7   =  "Билирубин прямой"  
  8   =  "АЛТ"               
  9   =  "АСТ"               
  10  =  "ГГТ"               
  11  =  "Щелочная фосфотаза"
  12  =  "Калий"             
  13  =  "Натрий"            
  14  =  "Хлор"              
  ;
 value $ NormFmt						/* Format for Laboratory Norm Flag */
  "NORMAL"   = "Норма"
  "ABNORMAL" = "Отклонение"
  " "        = "н/п"
  ;
 value   DevFmt						/* Format for Deviations */
  1   =  "Несерьезное"
  2   =  "Серьезное"
  ;
 value   SympFmt						/* Format for Symptoms */
  1   =  "Отсутствует"
  2   =  "Средняя степень"
  3   =  "Тяжелая степень"
  ;
 value   DynFmt						/* Format for DynFmt MRI/US Parameters */
  1   =  "Пол"
  0   =  "Б/И"
 -1   =  "Отр"
  ;
 value   AESevFmt						/* Format for AE Sevire Degree */
  1   =  "Слабая"
  2   =  "Умеренная"
  3   =  "Тяжелая"
  ;
 value   AERelFmt						/* Format for AE Relationship with IP */
  1   =  "Не связано"
  2   =  "Связано"
  ;
 value   AEOutFmt						/* Format for AE Outcome */
  1   =  "Разрешилось"
  2   =  "Разрешилось с последствиями"
  3   =  "Не разрешилось"
  4   =  "Фатальное НЯ"
  5   =  "Неизвестно"
  ;
 value   IPActFmt						/* Format for IP Actions concerning with AE */
  1   =  "Не применялись"
  2   =  "Отмена препарата"
  ;
 value   HLAFlFmt						/* Format for HLA Flag */
  1   =  "Выполнено"
  0   =  "Не выполнено"
  .   =  "н/п"
  ;
 value   HLAFmt						/* Format for HLA */
  1   =  "Положительный"
  2   =  "Отрицательный"
  .   =  "н/п"
  ;
run;


%let Title   =  Перечень 16.1. Распределение пациентов;				/* Output Title */
%let Input   =  Screening;															/* Input  Data */
%let Output  =  Listing_1_Screening;											/* Output Data and File */
%let OutFile =  Приложение_21_Перечень_1_Распределение_пациентов;		/* Output File Name */

data			Libr.&Output.;
	set		Libr.&Input. ;
	format	CritFl  CritConf  YNFmt.
				InCritX ExCritX  $BlankFmt.
				FASFL   PPFL   NYFmt.;
*	keep		Screen_Num  Site_Num  ScrDT  ICFDT  CritFl  InCritX  ExCritX  CritConf  Status  FASFL  PPFL; 
run;

	%PrintOptionsL ();

proc report	data = Libr.&Output.
				style(report)={borderbottomwidth=2pt};
*				spanrows split="|" ;
*	column (	&Title1.  (Name  Number) );
	define Screen_Num / display style(column)={asis=off just=c width=08%} "Скрининговый номер";
	define Site_Num   / display style(column)={asis=off just=c width=06%} "Номер центра";
	define ScrDT      / display style(column)={asis=off just=c width=09%} "Дата визита";
	define ICFDT      / display style(column)={asis=off just=c width=09%} "Дата подписания ИС";
	define CritFl     / display style(column)={asis=off just=c width=08%} "Итоговая оценка критериев";
	define InCritX    / display style(column)={asis=off just=c width=09%} "Нарушенные критерии включения";
	define ExCritX    / display style(column)={asis=off just=c width=09%} "Нарушенные критерии исключения";
	define CritConf   / display style(column)={asis=off just=c width=08%} "Подтверждение критериев";
	define Status     / display style(column)={asis=off just=c width=16%} "Статус пациента";
	define FASFL      / display style(column)={asis=off just=c width=08%} "Популяция всех включенных пациентов";
	define PPFL       / display style(column)={asis=off just=c width=08%} "Популяция оценки клинического эффекта";
run;

ods rtf close;


%let Title   =  Перечень 16.2. Рандомизация пациентов;
%let Input   =  Randomization;
%let Output  =  Listing_2_Randomization;
%let OutFile =  Приложение_22_Перечень_2_Рандомизация_пациентов;

data			Libr.&Output.;
	set		Libr.&Input. ;
	format	CritConf   YNFmt.
				ReasXRand  $BlankFmt.
				Group      GrFmt.;
*				FASFL   PPFL   NYFmt.;
*	keep		Screen_Num  Site_Num  CritConf  ReasXRand  RandNum  Group /* FASFL  PPFL */ ; 
run;

	%PrintOptions ();

proc report	data = Libr.&Output.
				style(report)={borderbottomwidth=2pt};
	define Screen_Num / display style(column)={asis=off just=c width=16%} "Скрининговый номер";
	define Site_Num   / display style(column)={asis=off just=c width=16%} "Номер центра";
	define CritConf   / display style(column)={asis=off just=c width=16%} "Подтверждение критериев";
	define ReasXRand  / display style(column)={asis=off just=c width=16%} "Причина невозможности рандомизации";
	define RandNum    / display style(column)={asis=off just=c width=16%} "Рандомизационный номер";
	define Group      / display style(column)={asis=off just=c width=16%} "Препарат";
run;

ods rtf close;


%let Title   =  Перечень 16.3. Пациенты, выбывшие из исследования;
%let Input   =  Data_Withdrawals;
%let Output  =  Listing_3_Withdrawals;
%let OutFile =  Приложение_23_Перечень_3_Пациенты_выбывшие_из_исследования;

%let EmpNote =  Выбывшие пациенты отсутствуют;		/* Emptiness Notification */

data			Libr.&Output.;
	set		Libr.&Input. ;
	keep		Screen_Num
				DctReas1 - DctReas4
				DctReasOth;
	format	DctReas1 - DctReas4  NYFmt.
				DctReasOth       $BlankFmt.;
	where		StudyEnd = 1;
run;
											/* Empty-String File for Absence of Withdrawals */
/*
data			Libr.&Output.;
	length	Screen_Num $20
				DctReas1 - DctReas4 $8
				DctReasOth $200;
run;
*/
%macro ReportMacro ();
																	/* Finding Number of Obserbations in Analysed File */
	proc sql noprint;
		select count(*)
		into  :NObs
		from   Libr.&Output.;
	quit;

%*	%put &=NObs;
	
	%PrintOptions ();
	
  %if (&NObs ^= 0) %then %do;
	proc report	data = Libr.&Output.
					style(report)={borderbottomwidth=2pt};
		define Screen_Num / display style(column)={asis=on just=c width=16%} "Скрининговый номер";
		define DctReas1   / display style(column)={asis=on just=c width=16%} "Отзыв пациентом формы ИС";
		define DctReas2   / display style(column)={asis=on just=c width=16%}
										"Выявление в ходе исследования ВИЧ-инфекции, онкологических заболеваний";
		define DctReas3   / display style(column)={asis=on just=c width=16%} "Наступление беременности";
		define DctReas4   / display style(column)={asis=on just=c width=16%} "Другая причина";
		define DctReasOth / display style(column)={asis=on just=c width=16%} "Описание";
	run;
  %end;
  %else %do;
%*	ods rtf text = " ";  
	ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
  %end;
	
	ods rtf close;
%mend;

		%ReportMacro ();


%let Title   =  Перечень 16.4. Пациенты, завершившие исследование с отклонениями от протокола;
*%let Input  =  Data_Deviations_Cas;
*%let Output =  Listing_4_Deviations;
%let OutFile =  Приложение_24_Перечень_4_Пациенты_завершившие_исследование_с_отклонениями_от_протокола;
%let EmpNote =  Пациенты с отклонениями от протокола отсутствуют;
/*
data			Libr.&Output.;
	set		Libr.&Input. ;
	format	Visit   VisFmt.
				DevType DevFmt.;
run;
*/
											/* Empty-String File for Absence of Deviations */
/*
data			Libr.&Output.;
	length	Screen_Num  Site_Num  Visit  DevType  DevDescr  $1;
run;
*/

%macro ReportMacro ();
																	/* Finding Number of Obserbations in Analysed File */
%*	proc sql noprint;
%*		select count(*)
%*		into  :NObs
%*		from   Libr.&Output.;
%*	quit;
																	/* Specifying Zero Number of Obserbations */
	%let NObs = 0;
	
	%PrintOptions ();
	
  %if (&NObs ^= 0) %then %do;
	proc report	data = Libr.&Output.
					style(report)={borderbottomwidth=2pt};
		define Screen_Num / display style(column)={asis=off just=c width=09%} "Скрининговый номер";
		define Site_Num   / display style(column)={asis=off just=c width=08%} "Номер центра";
		define Visit      / display style(column)={asis=off just=c width=11%} "Визит";
		define DevType    / display style(column)={asis=off just=c width=14%} "Тип отклонения";
		define DevDescr   / display style(column)={asis=off just=j width=56%} "Описание отклонения";
	run;
  %end;
  %else %do;
	ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
  %end;
	
	ods rtf close;
%mend;

		%ReportMacro ();


%let Title   =  Перечень 16.5. Демография;
%let Input   =  ADSL;												
%let Output  =  Listing_5_Demography;							
%let OutFile =  Приложение_25_Перечень_5_Демография;

data			Libr.&Output.;
	set		Libr.&Input.;
	keep		Screen_Num
				Sex  Race  Race_Oth  Age;
	format	Sex        SexFmt.
				Race       RaceFmt.
				Race_Oth  $BlankFmt.
				Age        CommaX.2;
run;

	%PrintOptions ();
						;
proc report	data = Libr.&Output.
				style(report)={borderbottomwidth=2pt};
	define Screen_Num / display style(column)={asis=off just=c width=19%} "Скрининговый номер";
	define Sex        / display style(column)={asis=off just=c width=19%} "Пол";
	define Race       / display style(column)={asis=off just=c width=19%} "Раса";
	define Race_Oth   / display style(column)={asis=off just=c width=19%} "Другая";
	define Age        / display style(column)={asis=off just=c width=19%} "Возраст, лет";
run;

ods rtf close;


%let Title   =  Перечень 16.6. Жизненно-важные показатели. Артериальное давление;
%let Input   =  Data_VS;
%let Output  =  Listing_6_VS_BP;
%let OutFile =  Приложение_26_Перечень_6_Жизненно_важные_показатели_Артериальное_давление;

%macro Listing_VS_BP ();
															/* Initializing Output Dataset with Empty Line */
			data			Libr.&Output.;
			run;

	%do k = 1 %to 1; /* ! *//*  &NVS */
		%do i = 1 %to &NV;

			data			Work.&Temp. ;
				set		Libr.&Input.;
				
				keep		Screen_Num
							Visit
						/*	Param */
							Col0 - Col&NVSType.	/* ! */
							; 
				format	Visit  VisFmt.
						/*	Param  VSFmt. */
							Col0 
							Col1   3.
							Col2 $ NormFmt.
							Col3 $ YNFmt.
							;

							Visit = &i.;
%*							Param = &k.;

					%do j = 1 %to 1; /* ! */
							Col0   = V&i._VS&&VSType&j.._&&VSPar&k..S ;
							Col1   = V&i._VS&&VSType&j.._&&VSPar&k..D ;
					%end;
					%do j = 2 %to &NVSType.; /* ! */
							Col&j. = V&i._VS&&VSType&j.._&&VSPar&k..  ;
					%end;
			run;
		  
			data			Libr.&Output.;
				set		Libr.&Output.
							Work.&Temp.  ;
			run;
		%end;
	%end;

			proc sort	data = Libr.&Output.;
				by			Screen_Num
							Visit
						/*	Param */
							;
				where		Screen_Num is not missing;
%mend;

	%Listing_VS_BP ();

	%PrintOptions ();

proc report	data = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=16%} "Скрининговый номер";
	define Visit      / display style(column)={asis=off just=c width=16%} "Визит";
/*	define Param      / display style(column)={asis=off just=l width=20%} "Параметр"; */
	define Col0       / display style(column)={asis=off just=c width=16%} "Систолическое АД, мм.рт.ст.";
	define Col1       / display style(column)={asis=off just=c width=16%} "Диастолическое АД, мм.рт.ст";
	define Col2       / display style(column)={asis=off just=c width=16%} "Оценка результата";
	define Col3       / display style(column)={asis=on  just=c width=16%} "Значимость отклонения";
run;

ods rtf close;


%let Title   =  Перечень 16.7. Жизненно-важные показатели.
 Частота сердечных сокращений, частота дыхательных движений, температура тела;
%let Input   =  Data_VS;
%let Output  =  Listing_7_VS_Main;
%let OutFile =  Приложение_27_Перечень_7_Жизненно_важные_показатели_ЧСС_ЧДД_температура;

%macro Listing_VS_BP ();
															/* Initializing Output Dataset with Empty Line */
			data			Libr.&Output.;
			run;

	%do k = 2 %to &NVS; /* ! */
		%do i = 1 %to &NV;

			data			Work.&Temp. ;
				set		Libr.&Input.;
				
				keep		Screen_Num
							Visit
							Param
							Col1 - Col&NVSType.
							; 
				format	Visit  VisFmt.
							Param  VSFmt.
						/*	Col1   CommaX.1 */
							Col1   Best.
							Col2 $ NormFmt.
							Col3 $ YNFmt.
							;

							Visit = &i.;
							Param = &k.;

					%do j = 1 %to &NVSType.;
							Col&j. = V&i._VS&&VSType&j.._&&VSPar&k..  ;
					%end;
			run;
		  
			data			Libr.&Output.;
				set		Libr.&Output.
							Work.&Temp.  ;
			run;
		%end;
	%end;

			proc sort	data = Libr.&Output.;
				by			Screen_Num
							Visit
							Param
							;
				where		Screen_Num is not missing;
%mend;

	%Listing_VS_BP ();

	%PrintOptions ();

proc report	data = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=16%} "Скрининговый номер";
	define Visit      / display style(column)={asis=off just=c width=16%} "Визит";
	define Param      / display style(column)={asis=off just=l width=16%} "Параметр";
	define Col1       / display style(column)={asis=off just=c width=16%} "Результат";
	define Col2       / display style(column)={asis=off just=c width=16%} "Оценка результата";
	define Col3       / display style(column)={asis=on  just=c width=16%} "Значимость отклонения";
run;

ods rtf close;


%let Title   =  Перечень 16.8. Физикальный осмотр;
%let Input   =  Data_PE;
%let Output  =  Listing_8_PE;
%let OutFile =  Приложение_28_Перечень_8_Физикальный_осмотр;

%macro Listing_PE ();
															/* Initializing Output Dataset with Empty Line */
			data			Libr.&Output.;
			run;

	%do k = 1 %to &NPE;
		%do i = 1 %to &NV;

			data			Work.&Temp. ;
				set		Libr.&Input.;
				
				keep		Screen_Num
							Visit  Param
							Col1 - Col&NPEType.
							; 
				format	Visit  VisFmt.
							Param  PEFmt.
							Col1 $ NormFmt.
							Col2 $ YNFmt.
/*							Col3 $ BlankFmt.*/
							Col3 $ 500.
							;

							Visit = &i.;
							Param = &k.;

					%do j = 1 %to &NPEType.;
							Col&j. = V&i._PE&&PEType&j.._&&PEPar&k.. ;
					%end;
			run;
		  
			data			Libr.&Output.;
				set		Libr.&Output.
							Work.&Temp.  ;
			run;
		%end;
	%end;

			proc sort	data = Libr.&Output.;
				by			Screen_Num
							Visit  Param;
				where		Screen_Num is not missing;
%mend;

	%Listing_PE ();

	%PrintOptionsL ();

proc report	data = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=10%} "Скрининговый номер";
	define Visit      / display style(column)={asis=off just=c width=08%} "Визит";
	define Param      / display style(column)={asis=off just=l width=20%} "Параметр";
	define Col1       / display style(column)={asis=off just=c width=08%} "Результат";
	define Col2       / display style(column)={asis=off just=c width=10%} "Значимость отклонения";
	define Col3       / display style(column)={asis=on  just=c width=42%} "Описание отклонения";
run;

ods rtf close;


%let Title   =  Перечень 16.9. Анамнез основного заболевания;
%let Input   =  Data_MD;
%let Output  =  Listing_9_MD;							
%let OutFile =  Приложение_29_Перечень_9_Анамнез_основного_заболевания;

data			Libr.&Output.;
	set		Libr.&Input. ;
	keep		Screen_Num
				DurMD  SevMD
				;
	format	DurMD  CommaX.1
				SevMD  MDSevFmt.
				;
run;

	%PrintOptions ();

proc report	data = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=33%} "Скрининговый номер";
	define DurMD      / display style(column)={asis=off just=c width=33%} "Продолжительность симптомов, часы";
	define SevMD      / display style(column)={asis=off just=c width=33%} "Степень тяжести";
run;

	ods rtf close;


%let Title   =  Перечень 16.10. Сопутствующие заболевания;
%let Input   =  Data_MH;
%let Output  =  Listing_10_MH;							
%let OutFile =  Приложение_30_Перечень_10_Сопутствующие_заболевания;
%let EmpNote =  Сопутствующие заболевания отсутствуют;

data			Libr.&Output.;
	set		Libr.&Input. ;
	keep		Screen_Num  MHTerm;
	where		MHTerm ^= "";
run;

%macro ReportMacro ();
																	/* Finding Number of Obserbations in Analysed File */
	proc sql noprint;
		select count(*)
		into  :NObs
		from   Libr.&Output.;
	quit;
																	/* Specifying Zero Number of Obserbations */
	%PrintOptions ();
	
  %if (&NObs ^= 0) %then %do;
	proc report	data = Libr.&Output.
					style(report)={borderbottomwidth=2pt};
		define Screen_Num / display style(column)={asis=off just=c width=29%} "Скрининговый номер";
		define MHTerm     / display style(column)={asis=off just=j width=70%} "Название заболевания";
	run;
  %end;
  %else %do;
	ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
  %end;
	
	ods rtf close;
%mend;

		%ReportMacro ();


%let Title   =  Перечень 16.11. Предшествующая и сопутствующая терапия;
%let Input   =  Data_CM;												
%let Output  =  Listing_11_CM;
%let OutFile =  Приложение_31_Перечень_11_Предшествующая_и_сопутствующая_терапия;

data			Libr.&Output.;
	set		Libr.&Input. ;
	keep		Screen_Num
				CMTRT        CMINDC									/* CM Treatment, Indications */
				CMSTDAT_DAY  CMSTDAT_MONTH  CMSTDAT_YEAR		/* Start Day, Month, Year    */
				CMENDAT_DAY  CMENDAT_MONTH  CMENDAT_YEAR;
	format	CMSTDAT_DAY  CMSTDAT_MONTH  CMSTDAT_YEAR    
				CMENDAT_DAY  CMENDAT_MONTH  CMENDAT_YEAR;
*				$ UnDateFmt.;
	where		CMTRT ^= "";
run;

	%PrintOptions ();

proc report	data = Libr.&Output.
				style(report)={borderbottomwidth=2pt};
	column (	("Скрининговый номер"  Screen_Num                               )
				("Препарат"           (CMTRT        CMINDC                     ))
				("Начало приёма"      (CMSTDAT_DAY  CMSTDAT_MONTH  CMSTDAT_YEAR))
				("Окончание приёма"   (CMENDAT_DAY  CMENDAT_MONTH  CMENDAT_YEAR)) );
	define Screen_Num    / display style(column)={asis=off just=c width=10%} "";
	define CMTRT         / display style(column)={asis=off just=c width=15%} "Название" ;
	define CMINDC        / display style(column)={asis=off just=c width=15%} "Показание к применению";
	define CMSTDAT_DAY   / display style(column)={asis=off just=c width=09%} "День" ;
	define CMSTDAT_MONTH / display style(column)={asis=off just=c width=09%} "Месяц";
	define CMSTDAT_YEAR  / display style(column)={asis=off just=c width=09%} "Год"  ;
	define CMENDAT_DAY   / display style(column)={asis=off just=c width=09%} "День" ;
	define CMENDAT_MONTH / display style(column)={asis=off just=c width=09%} "Месяц";
	define CMENDAT_YEAR  / display style(column)={asis=off just=c width=09%} "Год"  ;
run;

ods rtf close;


%let Title   =  Перечень 16.12. Клинический анализ крови;
%let Output  =  Listing_12_Lab_ClBlTest;
%let OutFile =  Приложение_32_Перечень_12_Клинический_анализ_крови;
%let EmpNote =  Клинический анализ крови не собирался;
		
	%PrintEmpNote ();

%let Title   =  Перечень 16.13. Биохимический анализ крови;
%let Output  =  Listing_13_Lab_BchBlTest;
%let OutFile =  Приложение_33_Перечень_13_Биохимический_анализ_крови;
%let EmpNote =  Биохимический анализ крови не собирался;
		
	%PrintEmpNote ();

%let Title   =  Перечень 16.14. Клинический анализ мочи;
%let Output  =  Listing_14_Lab_ClUrTest;
%let OutFile =  Приложение_34_Перечень_14_Клинический_анализ_мочи;
%let EmpNote =  Клинический анализ мочи не собирался;
		
	%PrintEmpNote ();


%let Title   =  Перечень 16.15. Оценка симптомов заболевания;
%let Input   =  Data_Symp;
%let Output  =  Listing_15_Symp;
%let OutFile =  Приложение_35_Перечень_15_Оценка_симптомов_заболевания;

%macro Listing_Symp ();
														/* Initializing Output Dataset with Empty Line */
		data			Libr.&Output.;
		run;
	
	%do i = 1 %to &NV;

		data			Work.&Temp. ;
			set		Libr.&Input.;
			
			keep		Screen_Num
						Visit
						Col1 - Col&NSymp.
						; 
			format	Visit  VisFmt.
					/*	Col1 - Col&NSymp.  SympFmt.*/
						;
			label		Col1  = "Чихание"
						Col2  = "Ринорея"
						Col3  = "Заложенность носа"
						Col4  = "Кашель"
						Col5  = "Лихорадка"
						Col6  = "Недомогание"
						Col7  = "Озноб"
						Col8  = "Головная боль"
						Col9  = "Миалгия"
						Col10 = "Боль в горле"
						Col11 = "Першение"
						Col12 = "Охриплость"
						Col13 = "Склерит/конъюнктивит"
						;

						Visit  = &i.;

				%do k = 1 %to &NSymp;
						Col&k. = V&i._&Symp.&k.  ;
				%end;
		run;
	  
		data			Libr.&Output.;
			set		Libr.&Output.
						Work.&Temp.  ;
		run;
	%end;

		proc sort	data = Libr.&Output.;
			by			Screen_Num
						Visit
						;
			where		Screen_Num is not missing;
%mend;

		%Listing_Symp ();

%macro ReportMacro ();

		%PrintOptionsL ();
	
	proc report	data  = Libr.&Output.
					split = "|" ;
		define Screen_Num / display style(column)={asis=off just=c width=06%} "Скрининговый номер";
		define Visit      / display style(column)={asis=off just=c width=06%} "Визит";
	 %do k = 1 %to &NSymp.;
		define Col&k.     / display style(column)={asis=off just=c width=06%} ;
	 %end;
	run;
		ods rtf close;
%mend;

		%ReportMacro ();


%let Title   =  Перечень 16.16. Пациенты с осложнениями гриппа и ОРВИ;
%let Output  =  Listing_16_Complications;
%let OutFile =  Приложение_36_Перечень_16_Осложнения_течения_заболевания;
%let EmpNote =  Пациенты с осложнениями гриппа или ОРВИ отсутствуют;
		
	%PrintEmpNote ();


%let Title   =  Перечень 16.17. Приверженность терапии;
%let Input   =  Data_Comp;
%let Output  =  Listing_17_Comp;							
%let OutFile =  Приложение_37_Перечень_17_Приверженность_терапии;

data			Libr.&Output.;
	set		Libr.&Input. ;
	keep		Screen_Num  Comp;
run;

	%PrintOptions ();

proc report	data  = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=49%} "Скрининговый номер";
	define Comp       / display style(column)={asis=off just=c width=49%} "Соблюдение режима лечения, %";
run;

	ods rtf close;


%let Title   =  Перечень 16.18. Нежелательные явления;
*%let Input  =  Data_AE;
*%let Output =  Listing_18_AE;
%let OutFile =  Приложение_38_Перечень_18_Нежелательные_явления;
%let EmpNote =  Нежелательные явления отсутствуют;
/*
%let VarAE   =  AETerm  AEStDat  AEEnDat  SevDeg  RelFl  AEOut  IPAct  SerFl;

data			Libr.&Output.;
	set		Libr.&Input. ;
	format	SevDeg  AESevFmt.
				RelFl   AERelFmt.
				AEOut   AEOutFmt.
				IPAct   IPActFmt.
				SerFl      YNFmt.;
	keep		Screen_Num  &VarAE; 
run;
*/

%macro ReportMacro ();
																	/* Finding Number of Obserbations in Analysed File */
%*	proc sql noprint;
%*		select count(*)
%*		into  :NObs
%*		from   Libr.&Output.;
%*	quit;
																	/* Specifying Zero Number of Obserbations */
	%let NObs = 0;
	
	%PrintOptions ();

%*	%PrintOptionsL ();
	
  %if (&NObs ^= 0) %then %do;
	proc report	data = Libr.&Output.
					style(report)={borderbottomwidth=2pt};
		define Screen_Num / display style(column)={asis=off just=c width=09%} "Скрининговый номер";
		define AETerm     / display style(column)={asis=off just=c width=25%} "Нежелательное явление"; /* "НЯ" */
		define AEStDat    / display style(column)={asis=off just=c width=09%} "Дата начала";
		define AEEnDat    / display style(column)={asis=off just=c width=09%} "Дата разрешения";
		define SevDeg     / display style(column)={asis=off just=c width=09%} "Степень тяжести";
		define RelFl      / display style(column)={asis=off just=c width=09%} "Связь с ИП";
		define AEOut      / display style(column)={asis=off just=c width=11%} "Исход";
		define IPAct      / display style(column)={asis=off just=c width=11%} "Меры в отношении ИП"; /* ", принятые" */
		define SerFl      / display style(column)={asis=off just=c width=05%} "Это СНЯ?";
	run;
  %end;
  %else %do;
	ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
  %end;
	
	ods rtf close;
%mend;

		%ReportMacro ();


			/* Non-using Code Begin */

%let Title   =  Перечень 16.XX. Биохимический анализ крови;
%let Input   =  Data_Lab;
%let Output  =  Listing_XX_Lab;
%let OutFile =  Приложение_XX_Перечень_XX_Биохимический_анализ_крови;

%macro Listing_Lab ();
															/* Initializing Output Dataset with Empty Line */
			data			Libr.&Output.;
			run;

	%do k = 1 %to &NLab;
		%do i = 1 %to &NV %by &LabSt;

			data			Work.&Temp. ;
				set		Libr.&Input.;
				
				keep		Screen_Num
							Visit  Param
							Col1 - Col&NTLab.
							; 
				format	Visit  VisFmt.
							Param  LabFmt.
							Col1   CommaX6.2
							Col2 $ 10.
							Col3 $ NormFmt.
							Col4 $ YNFmt.
							;

							Visit = &i.;
							Param = &k.;

					%do j = 1 %to &NTLab;
							Col&j. = V&i._&&Type&j.._&&Par&k.. ;
					%end;
			run;
		  
			data			Libr.&Output.;
				set		Libr.&Output.
							Work.&Temp.  ;
			run;
		%end;
	%end;

			proc sort	data = Libr.&Output.;
				by			Screen_Num
							Visit  Param;
				where		Screen_Num is not missing;
%mend;
/*
	%Listing_Lab ();

	%PrintOptions ();

proc report	data = Libr.&Output.;
	define Screen_Num / display style(column)={asis=off just=c width=13%} "Скрининговый номер";
	define Visit      / display style(column)={asis=off just=c width=12%} "Визит";
	define Param      / display style(column)={asis=off just=l width=21%} "Параметр";
	define Col1       / display style(column)={asis=on  just=c width=13%} "Результат";
	define Col2       / display style(column)={asis=off just=l width=13%} "Единицы измерения";
	define Col3       / display style(column)={asis=off just=c width=13%} "Оценка";
	define Col4       / display style(column)={asis=off just=c width=13%} "Значимость отклонения";
run;

ods rtf close;
*/
			/* Non-using Code End */
