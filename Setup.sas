														/* Setup.sas */
											/* Document template creation */

ods  escapechar = "#";
%let Tab = #{nbspace 4};

proc template;

 %let fontsize=12pt; *12pt;
 %let fontsizetable=11pt; *12pt;

 %let fonttype="Times New Roman";

 define style sarods;
 parent=styles.rtf;
 
 style contenttitle from indextitle /
  outputwidth=100%
  textalign=center
  font = (&fonttype, &fontsize);
  
 style body from document / font = (&fonttype,&fontsize) 
  leftmargin   = 2.5cm 
  rightmargin  = 1.0cm 
  topmargin    = 1.0cm 
  bottommargin = 1.0cm;

 style contents / background=white font = (&fonttype,&fontsize); 

**** Styles in the tables;

 style usertext / font = (&fonttype,&fontsize) outputwidth=100% ; 
 style systemtitle / foreground=black font = (&fonttype,10pt) font_weight=bold; 
 style systemfooter / foreground=black font = (&fonttype,10pt); 
 style data / background=white font = (&fonttype,&fontsizetable); *verticalalign=b; 
 style cell / background=white font = (&fonttype,&fontsizetable); 
 style table / background=white font = (&fonttype,&fontsizetable) frame= hsides rules=groups outputwidth=100% 
       cellspacing=0 cellpadding=1pt borderwidth=1pt bordertopwidth=2pt borderbottomwidth=1pt; 
 style byline from titlesandfooters "controls byline text." / font=(&fonttype,&fontsizetable); 
 style batch from batch / font = (&fonttype,&fontsizetable); 
 style header / background=white foreground=black font_weight=bold font=(&fonttype,&fontsizetable)
 		 borderbottomwidth=1pt verticalalign=c; *bold medium;
 style rowheader / background=white font_weight=bold font=(&fonttype,&fontsizetable)
		 verticalalign=c; *bold medium;
 end;

run; 

proc template;

 define style sarodsl;
 parent=sarods;
  
 style body from document / 
  leftmargin   = 1.0cm 
  rightmargin  = 1.0cm 
  topmargin    = 2.5cm 
  bottommargin = 1.0cm;
 end;

run;

%macro PrintOptions ();
	&TitleSAR;	&FootnoteSAR;

	options nodate nonumber byline orientation=portrait;
	ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data;

	ods rtf path = "&OutPath"
			  file = "&OutFile..rtf";
	
	ods rtf text = "#S={just=j}&Title."  startpage=now;  
	ods rtf text = "#S={fontsize=6pt}";
%mend;

%macro PrintOptionsL ();
	&TitleSAR;	&FootnoteSAR;

	options nodate nonumber byline orientation=landscape; /* landscape */
	ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarodsl contents=no notoc_data; /* sarodsl */

	ods rtf path = "&OutPath"
			  file = "&OutFile..rtf";
	
	ods rtf text = "#S={just=j}&Title."  startpage=now;  
	ods rtf text = "#S={fontsize=6pt}";
%mend;

%macro PrintOptions1 ();
	&TitleSAR;	&FootnoteSAR;

	options nodate nonumber byline orientation=portrait; /* portrait */
	ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data; /* sarods */

	ods rtf path = "&OutPath"
			  file = "&OutFile..rtf";
%mend;

%macro PrintOptionsL1 ();
	&TitleSAR;	&FootnoteSAR;

	options nodate nonumber byline orientation=landscape; /* landscape */
	ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarodsl contents=no notoc_data; /* sarodsl */

	ods rtf path = "&OutPath"
			  file = "&OutFile..rtf";
%mend;

%macro PrintEmpNote ();
	&TitleSAR;	&FootnoteSAR;

	options nodate nonumber byline orientation=portrait;
	ods rtf file = "&OutPath\&OutFile..rtf" startpage=yes style=sarods contents=no notoc_data;

	ods rtf path = "&OutPath"
			  body = "&OutFile..rtf";
	
	ods rtf text = "#S={just=j}&Title."  startpage=now;  
	ods rtf text = "#S={fontsize=6pt}";
	ods rtf text = "#S={fontsize=11pt just=c font_weight=bold}&EmpNote.";  
	
	ods rtf close;
%mend;
